-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 
-- Версия на сървъра: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gamehub`
--

-- --------------------------------------------------------

--
-- Структура на таблица `games`
--

CREATE TABLE `games` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `ended` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='where we store data every time a user starts a game';

-- --------------------------------------------------------

--
-- Структура на таблица `games-questions`
--

CREATE TABLE `games-questions` (
  `gameId` int(11) NOT NULL,
  `questionId` int(11) NOT NULL,
  `answer` int(1) NOT NULL DEFAULT '0',
  `timeStarted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `timeFinished` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='questions used in user''s game';

-- --------------------------------------------------------

--
-- Структура на таблица `questions`
--

CREATE TABLE `questions` (
  `id` int(16) NOT NULL,
  `question` varchar(20) NOT NULL,
  `answer1` varchar(20) NOT NULL,
  `answer2` varchar(20) NOT NULL,
  `answer3` varchar(20) NOT NULL,
  `answer4` varchar(20) NOT NULL,
  `correctAnswer` varchar(2) NOT NULL,
  `difficulty` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Схема на данните от таблица `questions`
--

INSERT INTO `questions` (`id`, `question`, `answer1`, `answer2`, `answer3`, `answer4`, `correctAnswer`, `difficulty`) VALUES
(1, 'Question1', 'a', 'b', 'c', 'dds', '2', '1'),
(2, 'Question11', 'a', 'b', 'c', 'd', '2', '11'),
(3, 'Question10', 'a', 'b', 'c', 'd', '2', '10'),
(4, 'Question9', 'a', 'b', 'c', 'd', '3', '9'),
(5, 'Question8', 'a', 'b', 'c', 'd', '3', '8'),
(6, 'Question7', 'a', 'b', 'c', 'd', '2', '7'),
(7, 'Question6', 'a', 'b', 'c', 'd', '3', '6'),
(8, 'Question5', 'a', 'b', 'c', 'd', '2', '5'),
(9, 'Question4', 'a', 'b', 'cs', 'd', '2', '4'),
(10, 'Question3', 'a', 'b', 'cs', 'd', '3', '3'),
(16, 'Question2', 'wqdqwdwdqwdw', 'dqwdwqdqw', 'dwdqdwqdwq', '1', '4', '2'),
(17, 'Question12', '1', '2', '3', '4', '2', '12'),
(18, 'question13', 'da', 'mnogo qsno', 'razbirase', 'estestveno', '1', '13');

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `question` varchar(255) CHARACTER SET utf8 NOT NULL,
  `answer` varchar(255) CHARACTER SET utf8 NOT NULL,
  `lastlogin` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isAdmin` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `question`, `answer`, `lastlogin`, `created`, `isAdmin`) VALUES
(1, 'dimsnxxx', 'dimanneeqk', 'stas_uzunov@hotmail.com', 'diman qk li e', 'ne', '2017-05-25 13:17:39', '2017-03-04 19:33:47', 1);

-- --------------------------------------------------------

--
-- Структура на таблица `users-statistics`
--

CREATE TABLE `users-statistics` (
  `id` int(4) NOT NULL,
  `username` varchar(15) NOT NULL,
  `gamesPlayed` int(4) NOT NULL DEFAULT '0',
  `gamesLost` int(4) NOT NULL DEFAULT '0',
  `gamesWon` int(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Схема на данните от таблица `users-statistics`
--

INSERT INTO `users-statistics` (`id`, `username`, `gamesPlayed`, `gamesLost`, `gamesWon`) VALUES
(1, 'dimsnxxx', 0, 0, 0),
(5, 'stas', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `games-questions`
--
ALTER TABLE `games-questions`
  ADD UNIQUE KEY `unique_index` (`gameId`,`questionId`),
  ADD KEY `questionId` (`questionId`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users-statistics`
--
ALTER TABLE `users-statistics`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `users-statistics`
--
ALTER TABLE `users-statistics`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `games_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`id`);

--
-- Ограничения за таблица `games-questions`
--
ALTER TABLE `games-questions`
  ADD CONSTRAINT `games-questions_ibfk_1` FOREIGN KEY (`gameId`) REFERENCES `games` (`id`),
  ADD CONSTRAINT `games-questions_ibfk_2` FOREIGN KEY (`questionId`) REFERENCES `questions` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
