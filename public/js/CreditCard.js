var CreditCard = (function() {
	
	// constructor
	function Constructor(name, age, gender) {
		// private vars
		var code = Constructor.generateCardCode(),
			released = false,
			locked = false,	
			money = 0,
			attempts = 0;	
		// public vars
			
		// public methods
		this.getCode = function() {
			if(!released) {
				released = true;
				return code;
			}
		}
		
		this.isLocked = function() {
			return locked;
		}
		
		this.getMoney = function(num, pincode) {
			if(!locked) {
				attempts++;
				pincode = pincode || prompt('enter pin code please: ');
				if(code === pincode) {
					attempts = 0;
					if(!isNaN(num) && num > 0) {
						if(money < num) {
							return -1;
						}
						money -= Number(num);
						return num;
					}
				}
				if (attempts >= 3) {
					console.log('Too much wrong attempts. Credit card is locked!');
					locked = true;
				}
			} else {
				console.log('Sorry this credit card is locked!');
			}
			return false;
		}
		
		this.putMoney = function(num) {
			if(!isNaN(num) && num > 0) {
				money += Number(num);
			}
		}
		
		this.checkMoney = function() {
			return money;
		}
		// private methods
		
	}
	
	(function() {
		// private static vars
		
		// public static vars
		
		// public static methods
		Constructor.generateCardCode = function() {
			return '' + Math.round(Math.random()*9) +
				Math.round(Math.random()*9) +
				Math.round(Math.random()*9) +
				Math.round(Math.random()*9);
		}
		

	})();

	
	return Constructor;
})();