function Timer(settings) {
    var timer = this,
        ppc = $(settings.pieChart || '.progress-pie-chart'),
        fill = $(settings.progressFill || '.ppc-progress-fill'),
        text = $(settings.progressPercent || '.ppc-percents span'),
        timeTotal = settings.timeTotal || 10,
        timeStarted = settings.timeStarted || 0,
        timeElapsed,
        timeStart,
        interval,
        timerWwtbam = document.getElementsByClassName('timer');

    this.getMinMax = function(num, min, max, method) {
        return isNaN(num) ? min || 0 : Math.min(Math.max(method ? method(num) : num, min || 0), max || 0);
    };
    this.updatePercent = function() {
        timeElapsed = (Date.now() - timeStart)/1000;
        this.setPercent(timeElapsed);
        if (timeElapsed >= timeTotal) {
            clearInterval(interval);
            if (typeof(settings.onTimerEnd) === 'function') {
                settings.onTimerEnd();
            }
        }
    };
    this.setPercent = function(timeElapsed) {
        timeElapsed = this.getMinMax(timeElapsed, 0, timeTotal);
        var deg = 360 * (timeElapsed / timeTotal),f=true;
        ppc.toggleClass(settings.halfClass || 'timer-half', timeElapsed >= timeTotal/2);
        fill.css("transform", "rotate(" + deg + "deg)");
        text.html(timeTotal - Math.floor(timeElapsed));
    };

    this.resetTimer = function () {
        timeStarted = 0;
        timeStart = Date.now() - timeStarted;
        this.updatePercent();
    };

    this.start = function() {
        timeStart = Date.now() - timeStarted;
        this.stop();
        interval = setInterval(function() {
            timer.updatePercent();
        }, 50);
    };

    this.stop = function() {
        clearInterval(interval);
        timer.updatePercent();
    };
}
