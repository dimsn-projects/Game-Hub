(function($) {
    "use strict";

    window.gamehub = {
        // templates
        tmpl: function(tmpl, props) {
            for (var i = 0, keys = Object.keys(props), len = keys.length; i < len; i++) {
                tmpl = (tmpl || '').replace(new RegExp('{' + keys[i].toUpperCase() + '}', 'g'), props[keys[i]]);
            }
            return tmpl;
        },

        getTemplate: function(name, options) {
            var el = $(this.tmpl('#{NAME}_tmpl', { name: name }));
            if (!el.length) {
                $log('Template not found: ' + tmpl)
            }
            return el.tmpl(options || {});
        },

        sendRequest: function(settings, data, success, error, always) {
            var request = {
                url: settings.url || settings,
                data: data,
                complete: false,
                success: success,
                error: error,
                always: always
            };
            var ajaxObj = {
                url: settings.url || settings,
                data: data,
                method: settings.method || 'POST',
                dataType: settings.dataType || 'json',
                success: function(data) {
                    try { request.success(data); } catch(err){}
                },
                error: function() {
                    try { request.error(); } catch(err){}
                }
            };
            for (var prop in settings) {
                if (settings.hasOwnProperty(prop)) {
                    ajaxObj[prop] = settings[prop];
                }
            }
            $.ajax(ajaxObj).always(function() {
                request.complete = true;
                try { request.always(); } catch(err){}
            });
            return request;
        }
    };
})(jQuery);