
$(document).ready(function() {
    $.fn.dataTable.ext.errMode = 'none';
    var table = $('table.statistics-table');
    table.DataTable({
        autoWidth: false
    }).on('error.dt', function(e, settings, techNote, message) {
        console.log('An error has been reported by DataTables: ', message);
    });
    //table.show();
    /*{
        paging:   true,
            ordering: true,
            info:     false,
            select: true
    }*/
});