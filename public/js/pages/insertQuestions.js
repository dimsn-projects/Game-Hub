(function($) {
    "use strict";
    var form = $('form.upload-form'),
        btn = form.find('input[type="submit"]'),
        questionsEl = $('.questions'),
        currentFile,
        formEnabled = true,
        jsonFiles;

    function insertQuestions() {
        form.hide();
        questionsEl.show();
        currentFile = 0;
        sendInsertRequest();
    }

    function sendInsertRequest() {
        if (jsonFiles[currentFile]) {
            window.gamehub.sendRequest('/insert/questions', { file: jsonFiles[currentFile].path }, onInsertSuccess, onInsertError);
        } else {
            console.log('no file!');
        }
    }

    function onInsertSuccess(data) {
        if (!data.error) {
            var fileObj = jsonFiles[currentFile];
            fileObj.count = data.count;
            fileObj.el.find('.count').text(data.count);
            if (fileObj.count >= fileObj.total) {
                currentFile++;
            }
            if (jsonFiles[currentFile]) {
                sendInsertRequest();
            } else {
                console.log('All questions are inserted');
            }
        } else {
            console.log('ERROR: ' + data.error);
        }
    }

    function onInsertError() {
        console.log('onInsertError');
        setTimeout(sendInsertRequest, 1000);
    }

    function updateForm(bool) {
        formEnabled = bool;
        btn.prop('disabled', !formEnabled);
    }

    function onFileSuccess(data) {
        console.log('onFileSuccess', data);
        jsonFiles = [];
        for (var i = 0; i < data.length; i++) {
            if (!data[i].error) {
                jsonFiles.push({
                    el: window.gamehub.getTemplate('questionsFile', { filename: data[i].name, count: 0, total: data[i].total }).appendTo(questionsEl),
                    count: 0,
                    total: data[i].total,
                    path: data[i].path
                });
            } else {
                console.log(data[i].name + ': ' + data[i].error);
            }
        }
        insertQuestions();
    }
    function onFileError() {
        console.log('onFileError');
        updateForm(true);
    }

    function onSubmit(event) {
        console.log('onSubmit');
        event.preventDefault();
        if (formEnabled) {
            updateForm(false);
            var formData = new FormData(form.get(0));
            form.find(':file').each(function() {
                var files = this.files;
                for (var i = 0; i < files.length; i++) {
                    console.log('append ' + this.name + '=' + files[i].name);
                    formData.append(this.name + '[]', files[i]);
                }
            });
            var settings = {
                url: form.attr('action'),
                contentType: false,
                processData: false
            };
            window.gamehub.sendRequest(settings, formData, onFileSuccess, onFileError);
        }
    }

    form.on('submit', onSubmit);
    questionsEl.hide();
})(jQuery);