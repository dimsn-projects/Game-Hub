(function ($) {
    var sounds = [
            {
                "name": "intro",
                "url": "/sound/wwtbam/intro.mp3"
            },
            {
                "name": "joker_1",
                "url": "/sound/wwtbam/joker_1.mp3"
            },
            {
                "name": "joker_2",
                "url": "/sound/wwtbam/joker_2.mp3"
            },
            {
                "name": "joker_3",
                "url": "/sound/wwtbam/joker_3.mp3"
            },
            {
                "name": "fifty_fifty",
                "url": "/sound/wwtbam/fifty_fifty.mp3"
            },
            {
                "name": "phone_loop",
                "url": "/sound/wwtbam/telefonjoker_loop.mp3"
            },
            {
                "name": "auswahlrunde_loesung",
                "url": "/sound/wwtbam/auswahlrunde_loesung.mp3"
            }
        ],
        soundsReady = 0,
        progressBarEl = $('.progress-bar'),
        progressBarLine = progressBarEl.find('.progress-line'),
        progressBarText = progressBarEl.find('.progress-text').text('0%');

    /*function sleep(ms) {
     return new Promise(resolve => setTimeout(resolve, ms));
     }*/

    /*function onSoundReady(name) {
     scount++;
     scountEl.text(Math.ceil((scount/sounds.length)*100) + "%");
     barEl.style.width = progressWidth/width + 'px';
     width--;
     console.log('Can play sound "' + name + '"');
     }*/

    function playSoundTime(name, ms) {
        var sound = sounds.find(function (sound) {
            return sound.name === name;
        });
        console.log('time started ' + sound.audio.duration);
        var timeStarted = Date.now();
        sound.audio.loop = true;
        sound.audio.addEventListener('timeupdate', function () {
            if (Date.now() - timeStarted >= ms) {
                sound.audio.pause();
                sound.currentTime = 0;
                console.log('time finish');
                sound.audio.ontimeupdate = undefined;
            }
        });
        sound.audio.play();
    }

    function getSoundReadyEvent(soundObj) {
        return function () {
            soundObj.audio.oncanplaythrough = undefined;
            soundObj.loaded = true;
            soundsReady++;
            console.log(soundsReady + '/' + sounds.length + ' name: ' + soundObj.name);
            var percent = Math.floor((soundsReady / sounds.length) * 100);
            progressBarText.text(percent + '%');
            progressBarLine.css('width', percent + '%');
            if (soundsReady === sounds.length) {
                playSoundTime("auswahlrunde_loesung", 15000);
            }
        }
    }



    function preloadSounds() {
        soundsReady = 0;
        for(var i = 0; i < sounds.length; i++) {
            sounds[i].audio = document.createElement('audio');
            sounds[i].audio.preload = 'auto';
            sounds[i].audio.oncanplaythrough = getSoundReadyEvent(sounds[i]);
            sounds[i].audio.src = sounds[i].url;
        }
    }

    function playSound(name) {
        var sound = sounds.find(function(sound) {
            return sound.name === name;
        });
        if(sound && sound.loaded) {
            sound.audio.play();
        } else if(!sound) {
            console.log('Sound not found "' + name + '"');
        } else if(!sound.loaded) {
            console.log('Sound not loaded "' + name + '"');
        }
    }
    preloadSounds();
})(jQuery);