(function($) {
	var initTime = new Date().getTime();
	function log(str) {
		console.log((new Date().getTime() - initTime) + ': ' + str);
	}
	log('register.phtml init');
	$(document).ready(function(){
		log('register.phtml ready');
		
		var registerForm = $('.register.phtml-form');
		if (registerForm.size()) {
			log('register.phtml page');
			registerForm.on('submit', function(event) {
				if (!registerForm.find('input[name="username"]').val().replace(/ /g, '').length){
					alert('the name must be longer');
					event.preventDefault();
				}				
				if (registerForm.find('[name="password"]').val().replace(/ /g, '').length < 4) {
					alert('the password is too short');
					event.preventDefault();
				}
				if (!registerForm.find('[name="question"]').val().replace(/ /g, '').length) {
					alert('there is no question typed');
					event.preventDefault();
				}
				if (registerForm.find('[name="passwordagain"]').val() !== registerForm.find('[name="password"]').val() ) {
					alert('the passwords do not match');
					event.preventDefault();
				}
				if (registerForm.find('[name="e-mail"]').val().replace(/ /g, '').length < 3 ) {
					alert('the e-mail is too short');
					event.preventDefault();
				}
				if (!registerForm.find('[name="answer"]').val().replace(/ /g, '').length) {
					alert('there is no answer typed');
					event.preventDefault();
				}
			});
		} else {
			log('login page');
			
		}
	});
	
})(jQuery);