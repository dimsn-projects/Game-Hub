(function($) {
    "use strict";
    var topkaEl = $('div.topka'),
        topkaSize = topkaEl.width(),
        dragging = false,
        sendComplete = true,
        updateTimeout;


    function update() {
        updateTimeout = setTimeout(updateData, 1000);
    }

    function updateData() {
        //console.log('updateData');
        window.gamehub.sendRequest({ url: '/topka/position', method: 'GET' }, null, function(data) {
            if (!dragging) {
                //moveElement(topkaEl, data.x - topkaSize/2, data.y - topkaSize/2);
                //updateElement(topkaEl, data.x - topkaSize/2, data.y - topkaSize/2);
                topkaEl.data('x', data.x - topkaSize/2);
                topkaEl.data('y', data.y - topkaSize/2);
                update();
            }
        }, function() {
            if (!dragging) {
                update();
            }
        });
    }

    function cancelUpdate() {
        clearTimeout(updateTimeout);
    }

    function updateElement(el) {
        if (!dragging) {
            var top = parseInt(el.css('top')),
                left = parseInt(el.css('left')),
                x = el.data('x'),
                y = el.data('y');
            //console.log(top + ':' + left + ' ' + x + ':' + y);
            el.css('top', (top + (y - top)/10) + 'px');
            el.css('left', (left + (x - left)/10) + 'px');
            //console.log(' - ' + parseInt(el.css('top')) + ':' + parseInt(el.css('left')));
        }
        requestAnimationFrame(updateElement.bind(this, el));
    }

    function moveElement(el, x, y) {
        el.css('top', y + 'px');
        el.css('left', x + 'px');
    }

    function startDrag(event) {
        console.log('start x: ' + event.pageX + ' y: ' + event.pageY);
        dragging = true;
        cancelUpdate();
        moveElement(topkaEl, event.pageX - topkaSize/2, event.pageY - topkaSize/2);
        $(document).on('mousemove', onDrag);
    }

    function onDrag(event) {
        moveElement(topkaEl, event.pageX - topkaSize/2, event.pageY - topkaSize/2);
        if (sendComplete) {
            sendComplete = false;
            window.gamehub.sendRequest({url: '/topka/position'}, {x: event.pageX , y: event.pageY}, function() {
                sendComplete = true;
            }, function() {
                sendComplete = true;
            });
        }
    }
    function endDrag() {
        console.log('stop x: ' + event.pageX + ' y: ' + event.pageY);
        dragging = false;
        $(document).off('mousemove', onDrag);

        topkaEl.data('x', event.pageX - topkaSize/2);
        topkaEl.data('y', event.pageY - topkaSize/2);
        window.gamehub.sendRequest({url: '/topka/position'}, {x: event.pageX , y: event.pageY}, function() {
            update();
        }, function() {
            update();
        });
    }
    topkaEl.on('mousedown', startDrag);
    $(document).on('mouseup', endDrag);

    updateData();

    topkaEl.data('x', parseInt(topkaEl.css('left')) - topkaSize/2);
    topkaEl.data('y', parseInt(topkaEl.css('top')) - topkaSize/2);
    updateElement(topkaEl);

})(jQuery);
