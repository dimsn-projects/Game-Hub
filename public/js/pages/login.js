(function($) {
	var initTime = new Date().getTime();
	function log(str) {
		console.log((new Date().getTime() - initTime) + ': ' + str);
	}
	log('login init');
	$(document).ready(function(){
		log('login ready');
		
		var loginForm = $('.login-form');
		if (loginForm.size()) {
			log('login page');
			loginForm.on('submit', function(event) {
				if (!loginForm.find('input[name="username"]').val().replace(/ /g, '').length){
					alert('bad username');
					event.preventDefault();
				}				
				if (loginForm.find('[name="password"]').val().replace(/ /g, '').length < 4) {
					alert('bad password');
					event.preventDefault();
				}
			});
		} else {
			log('game page');
			
		}
	});
	
})(jQuery);