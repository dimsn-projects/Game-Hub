var User = (function() {
	
	// constructor
	function Constructor() {
		// private vars
		var cardCode;
		// public vars
		this.card = undefined;
		
		// public methods
		this.createCreditCard = function() {
			if(!this.card) {
				this.card = new CreditCard();
				cardCode = this.card.getCode();
			}
		};
		
		this.stealCreditCardFrom = function(user) {
			this.card = user.card;
			delete user.card;
		}
		
		this.getMoney = function(num) {
			if(this.card) {
				if (this.card.isLocked()) {
					console.log('credit card is already locked');
				} else {
					while(!this.card.isLocked()){
						var result = this.card.getMoney(num, cardCode || prompt('enter pin code please: '));
						switch(result) {
							case false:
								cardCode = undefined;
								break;
							case -1:
								console.log('no money');
								return 0;
							default:
								return result;
						}
					}
				}
			}
			else {
				console.log('You dont have a Credit card');
			}
			return 0;
		}
		
		this.putMoney = function(num) {
			this.card.putMoney(num || prompt('how much money'));
		}
		
		// private methods
		
		//init
		
	}
	
	return Constructor;
})();