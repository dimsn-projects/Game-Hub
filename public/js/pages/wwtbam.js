(function($) {
    "use strict";

    var form = $('form'),
        btn = form.find('button.submit'),
        questionsEl = $('.questions'),
        questionEl = questionsEl.find('.question.original span'),
        answerEls = questionsEl.find('label.answer'),
        answerTextEls = answerEls.find('span'),
        answersRadios = questionsEl.find('input[name="answer"]'),
        newGameBtn = $('.new-game-btn'),
        mainMenuBtn = $('.menu-btn'),
        endGameBox = $('.end-game-msg'),
        containerEl = $('.container'),
        timeOverEl = $('.time-over'),
        wrongAnswerEl = $('.wrong-answer'),
        inputAnswer,
        rightQuestionsEl = $('.wrong-answer span'),
        youWinEl = $('.you-win'),
        formEnabled = false,
        interactive = false,
        timer = null,
        newGameRequest,
        ladder = $('.ladder'),
        ladderSteps = ladder.find('li.animated h4'),
        bankEl = $('.bank'),
        bankMoneyEl = bankEl.find('.money'),
        currPrice = 50,
        steps = ladder.find('li'),
        jokerEls = $('.jokers ul'),
        msgElSpan = $('li.msg.questions .question span'),
        answer1El = answerEls.find('span.0'),
        answer2El = answerEls.find('span.1'),
        answer3El = questionsEl.find('label[data-value="3"]'),
        answer4El = questionsEl.find('label[data-value="4"]'),
        bankEl = $('.bank'),
        infoEl = $('.info'),
        infoBox = $('.help-info'),
        infoBoxCopy = $('.help-info-copy'),
        isInfoBoxShown = false,
        soundVolumeOnEl = $('.speaker-on'),
        soundVolumeOffEl = $('.speaker-off'),
        soundsReady = 0,
        areCreatedSounds = false,
        jokersDisabled = true,
        sounds = [
            {
                "name" : "game_end",
                "url" : "/sound/wwtbam/"
            },
            {
                "name" : "intro",
                "url" : "/sound/wwtbam/intro.mp3"
            },
            {
                "name": "joker_1",
                "url": "/sound/wwtbam/joker_1.mp3"
            },
            {
                "name": "joker_2",
                "url": "/sound/wwtbam/joker_2.mp3"
            },
            {
                "name": "joker_3",
                "url": "/sound/wwtbam/joker_3.mp3"
            },
            {
                "name": "fifty_fifty",
                "url": "/sound/wwtbam/fifty_fifty.mp3"
            },
            {
                "name": "phone_start",
                "url": "/sound/wwtbam/phone_start.mp3"
            },
            {
                "name": "phone_loop",
                "url": "/sound/wwtbam/phone_loop.mp3"
            },
            {
                "name": "phone_end",
                "url": "/sound/wwtbam/phone_end.mp3"
            },
            {
                "name": "candidate_selection_start",
                "url": "/sound/wwtbam/candidate_selection_start.mp3"
            },
            {
                "name": "candidate_selection_loop",
                "url": "/sound/wwtbam/candidate_selection_loop.mp3"
            },
            {
                "name": "correct_answer",
                "url": "/sound/wwtbam/richtig_stufe_3_letzte.mp3"
            },
            {
                "name": "time_refresh",
                "url": "/sound/wwtbam/time_refresh.mp3"
            },
            {
                "name": "false_answer",
                "url": "/sound/wwtbam/false_answer.mp3"
            }
        ],
        playlist1 = [
            {
                "name" : "intro",
                "loop" : "10000"
            },
            {
                "name" : "candidate_selection_solution",
                "loop" : "10000"
            }
        ],
        playlist2 = [
            {
                "name" : "phone_start",
                "loop" : undefined
            },
            {
                "name" : "phone_loop",
                "loop" : "30000"
            },
            {
                "name" : "phone_end",
                "loop" : undefined
            },
        ],
        trueAnswer,
        rightQuestions=0;
    
    function updateForm(bool) {
        formEnabled = bool;
        btn.prop('disabled', !formEnabled);
    }

    function isFormEnable() {
        return formEnabled;
    }

    // send request to start new game

    function checkJokers() {
        window.gamehub.sendRequest('/wwtbam', {} , onJokerSuccess, onJokerError);
        console.log('jokers checked');
    }
    function checkScore() {

    }
    function  checkMoney() {

    }
    // start the game
    function startGame() {
        //newGameBtn.hide();
        //mainMenuBtn.hide();
        jokerEls.addClass('disabled');
        checkJokers();
        //checkScore();
        //checkMoney();
        answerEls.removeClass('disabled');
        bankMoneyEl.innerHTML = '$0';
        bankMoneyEl.html('$0');
        $('#step15').removeClass('selected');
        infoBox.hide();
        infoBoxCopy.show();
        endGameBox.hide();
        console.log('startGame');
        timeOverEl.hide();
        wrongAnswerEl.hide();
        youWinEl.hide();
        containerEl.show();
        updateForm(false);
        interactive = true;
        startTimer();
    }

    function endGame(data, win) {
        console.log('EndGame win=' + win, data);

        containerEl.hide();

        jokerEls.removeClass('used'); // new

        if (data.trueAnswer) {
            console.log('true answer: ' + data.trueAnswer);
            //$('#answer' + data.trueAnswer).addClass('true-answer');
            trueAnswer = data.trueAnswer;
        }
        if (data.timeUp) {
            msgElSpan.text("Your time is up!");
        } else if (win) {
            msgElSpan.text("You win!");
        } else {
            rightQuestionsEl.text(rightQuestions);
            msgElSpan.text("Wrong answer! Correct answers: " + rightQuestions);
            rightQuestions = 0;
        }
        var animationName = 'animated bounceInDown';
        var animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
       /*newGameBtn.removeClass('animated fadeOut');
        newGameBtn.addClass(animationName).one(animationend, function () {
            newGameBtn.removeClass(animationName);
        });
        newGameBtn.show();*/
        /*mainMenuBtn.removeClass('animated fadeOut');
        mainMenuBtn.addClass(animationName).one(animationend, function () {
            mainMenuBtn.removeClass(animationName);
        });
        mainMenuBtn.show();*/
        endGameBox.removeClass('animated bounceOutUp');
        endGameBox.addClass(animationName).one(animationend, function () {
            endGameBox.removeClass(animationName);
        });
        endGameBox.show();
        infoBoxCopy.hide();
    }

    function nextQuestion(question) {
        formEnabled = true;
       // newGameBtn.hide();
       // mainMenuBtn.hide();
        endGameBox.hide();
        infoBoxCopy.show();
        console.log("next question");

        answersRadios.prop('checked', false);

        questionEl.text(question.question);
        answerTextEls.each(function(index) {
            $(this).text(question['answer' + (index + 1)]);
        });
        console.log(question);

        wwtbamData.trueAnswer = question.correctAnswer;

        console.log('new true answer: '+wwtbamData.trueAnswer);
        if(!jokersDisabled) {
            jokerEls.removeClass('disabled');
        } // new
        answerTextEls.removeClass('disabled'); // new
        answerEls.removeClass('disabled'); // new
        answersRadios.prop('disabled', false); // new

        $('#answer' + inputAnswer.val()).removeClass('true-answer');
    }

    function startTimer() {
        console.log('startTimer');
        if (!timer) {
            timer = new Timer({
                pieChart: '.progress-pie-chart',
                progressFill: '.ppc-progress-fill',
                progressPercent: '.ppc-percents span',
                halfClass: 'timer-half',
                timeTotal: window.wwtbamData.timeTotal,
                timeStarted: (window.wwtbamData.timeNow - window.wwtbamData.timeStarted)*1000,
                onTimerEnd: onTimerEnd
            });
        }
        timer.resetTimer();
        timer.start();
    }

    function onTimerEnd() {
        //console.log('onTimerEnd');
        containerEl.hide();
        timeOverEl.show();
       /* newGameBtn.show();
        mainMenuBtn.show(); */
       endGameBox.show();
       infoBoxCopy.hide();
        window.gamehub.sendRequest({ url: '/wwtbam/timeup', method: 'GET' });
    }

    // answer request
    function onAnswerSuccess(data) {
        console.log(data);
        console.log('onAnswerSuccess', data);
        questionsEl.addClass('checked');

        answerTextEls.removeClass('disabled'); // new
        //answerEls.removeClass('disabled'); // new


        var ms;
        if (data.ended) {
            console.log("end game");

            if(data.answer === data.trueAnswer) {
                currPrice = '1 MILLION';
                bankMoneyEl.innerHTML = '$' + currPrice;
                bankMoneyEl.html('$' + currPrice);
                $('#step14').removeClass('selected');
                $('#step15').addClass('selected');
                $('#answer' + inputAnswer.val()).addClass('true-answer');
                playSound('correct_answer');
                console.log('you win!');
            } else {
                $('#answer' + data.trueAnswer).addClass('true-answer');
                playSound('false_answer');
                answerEls.addClass('disabled');
            }
            ms = getSoundDuration('false_answer');
            setTimeout(function () {
                endGame(data, data.trueAnswer === data.answer);
            }, ms);
        } else if (data.nextQuestion) {
            console.log("next question");
            playSound('correct_answer');
            console.log("ready for next question...");
            $('#answer' + inputAnswer.val()).addClass('true-answer');

            ladderIncrease();
            console.log('after ladder increase');

            ms = getSoundDuration('correct_answer');
            rightQuestions += 1;
            console.log("right questions +1");
            //goUpper();
            console.log("ready for next question...");
            setTimeout(function () {
                nextQuestion(data.nextQuestion);
                startTimer();
            }, ms);
            interactive = true;
        }
    }

    function onNewGameSuccess(data) {
        console.log('onNewGameSuccess', data);

        jokersDisabled = false;

        for(let j = ladderSteps.length-1; j > 0; j--) {
            $('#step'+(j)).removeClass('selected');
        }
        currPrice = 50;
        questionsEl.removeClass('checked');
        answersRadios.prop('checked', false);
        msgElSpan.text('');
        if(data.nextQuestion) {
            console.log(data.nextQuestion);
            nextQuestion(data.nextQuestion);
            startGame();
        }
    }

    function onNewGameError(error) {
        console.log('onNewGameError', error);
    }

    function onAnswerError() {
        console.log('onAnswerError');
    }

    // event
    function onNewGame(event) {
        console.log('onNewGame');

        jokerEls.removeClass('disabled');

        var animationName = 'animated bounceOutUp';
        var animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

       /* newGameBtn.addClass(animationName).one(animationend, function () {
            newGameBtn.removeClass(animationName);
        });
        mainMenuBtn.addClass(animationName).one(animationend, function () {
            mainMenuBtn.removeClass(animationName);
        });*/
        endGameBox.addClass(animationName).one(animationend, function () {
            endGameBox.removeClass(animationName);
        });


        answersRadios.prop( "disabled", false);

        rightQuestions = 0;
        rightQuestionsEl.text(rightQuestions);
        $('#answer' + trueAnswer).removeClass('true-answer');
        event.preventDefault();
        if (!newGameRequest || newGameRequest.complete) {
            timer.resetTimer();
            newGameRequest = window.gamehub.sendRequest({ url: '/wwtbam/new', method: 'GET' }, null, onNewGameSuccess, onNewGameError);
        }
    }
    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    // on text clicks
    async function onLabelClick(event) {
        console.log("choose");

        jokerEls.addClass('disabled'); // new


        var answerName = $(this).attr('name');
        inputAnswer = answersRadios.filter("#"+answerName+"");

        inputAnswer.prop("checked", true); // new

        answerTextEls.addClass('disabled');
        answersRadios.prop('disabled', true);

        console.log(inputAnswer.val());
        if (!interactive) {
            if(event !== undefined)event.preventDefault();
        } else if (!isFormEnable()) {
            updateForm(true);
        }
        onSubmit()
    }

    function onSubmit(event) {
        console.log('onSubmit');
        if(event !== undefined)event.preventDefault();
        if(isFormEnable()) {
            console.log('timer stop');
            timer.stop();
            updateForm(false);
            interactive = false;
            var props = {};
            props['answer'] = inputAnswer.val();
            //console.log(inputAnswer);
            window.gamehub.sendRequest('/wwtbam', props, onAnswerSuccess, onAnswerError);
        }
    }
    // ladder
    function goUpper() {
        var step,
            current = -1;
        for (var i = steps.length - 1; i >= 0; i--) {
            step = steps.eq(i);
            if (step.hasClass('current')) {
                step.removeClass('current');
                current = i-1;
                break;
            }
        }
        steps.eq(current).addClass('current');
    }
    function onJokerSuccess(data) {
        console.log('onJokerSuccess');
        console.log(data);
        if(data.used) {
            var usedJoker = jokerEls.find('.' + data.type);
            console.log(usedJoker);
            usedJoker.addClass('used');
        }
        if(data.time && data.fifty && data.cut) {
            jokerEls.addClass('disabled');
        } else {
            jokersDisabled = false;
            jokerEls.removeClass('disabled');
            if(data.time === 1) {
                var usedJoker = jokerEls.find('.time');
                console.log(usedJoker);
                usedJoker.addClass('used');
            }
            if(data.fifty === 1) {
                var usedJoker = jokerEls.find('.fifty-fifty');
                console.log(usedJoker);
                usedJoker.addClass('used');
            }
            if(data.cut === 1) {
                var usedJoker = jokerEls.find('.cut');
                console.log(usedJoker);
                usedJoker.addClass('used');
            }
        }
    }
    function onJokerError(data) {
        console.log('onJokerError');
    }
    // jok3rs
    jokerEls.on('click', function (event) {
        var $this = $(this);
        if(!$this.hasClass('used')) {
            $this.addClass('used');
            console.log($this.data('type'));
            if($this.data('type') === "time") {
                playSound('time_refresh');
                startTimer();
            } else if($this.data('type') === "fifty-fifty") {
                playSound('fifty_fifty');
                var it = 0;
                answerEls.map(function (answer) {
                    answer+=1;
                    console.log('answer: '+answer);
                    console.log('correct answer: '+wwtbamData.trueAnswer);
                    if(Number(answer) !== Number(wwtbamData.trueAnswer) && it < 2) {
                        //gogo

                        var answerSpan = answerEls.find('span.'+answer+'');

                        answerSpan.addClass('disabled');

                        console.log(answerSpan);
                        answerSpan.text("");
                        it++;
                    } else if(Number(answer) === Number(wwtbamData.trueAnswer)) {
                        console.log('here');
                    }
                })
            } else if($this.data('type') === "cut") {
                inputAnswer = answersRadios.filter("#answer"+Number(wwtbamData.trueAnswer)+"");

                inputAnswer.prop("checked", true);

                answerTextEls.addClass('disabled');
                answersRadios.prop('disabled', true);

                if (!interactive) {
                    if(event !== undefined)event.preventDefault();
                } else if (!isFormEnable()) {
                    updateForm(true);
                }

                onSubmit();
            }
            if($this.data('type'))
            window.gamehub.sendRequest('/wwtbam', {type : $this.data('type')}, onJokerSuccess, onJokerError);
        } else {
            console.log("it's used");
        }
    });
    // sounds
    /*function preloadSounds() {
        sounds.map(function(sound) {
            function onSoundLoad(event) {
                sound.audio.removeEventListener('canplaythrough', onSoundLoad);
                console.log('Sound loaded "' + sound.name + '"');
                sound.loaded = true;
            }
            sound.audio = document.createElement('audio');
            sound.audio.preload = 'auto';
            sound.audio.addEventListener('canplaythrough', onSoundLoad);
            sound.audio.src = sound.url;
        });
    }*/
    function playSoundTime(name, ms, callback) {
        var sound = sounds.find(function (sound) {
            return sound.name === name;
        });
        console.log('time started ' + sound.audio.duration);
        var timeStarted = Date.now();
        sound.audio.currentTime = 0;

        setTimeout(function() {
            console.log('end');
            sound.audio.pause();
            clearTimeout(sound.loopTimeout);
            if (typeof callback === 'function') {
                try {
                    callback();
                } catch (err) {
                    console.log(err);
                }
            }
        }, ms);

        function loop() {
            sound.audio.pause();
            sound.audio.currentTime = 0;
            sound.loopTimeout = setTimeout(function() {
                console.log('loop');
                loop();
            }, sound.audio.duration*1000 - 100);
            sound.audio.play();
        }

        loop();
    }

    function getSoundReadyEvent(soundObj) {
        return function () {
            soundObj.audio.oncanplaythrough = undefined;
            soundObj.loaded = true;
            soundsReady++;
            //console.log(soundsReady + '/' + sounds.length + ' name: ' + soundObj.name);
            //var percent = Math.floor((soundsReady / sounds.length) * 100);
            //progressBarText.text(percent + '%');
            //progressBarLine.css('width', percent + '%');
            if (soundsReady === sounds.length) {
                //playlist(playlist1);
                console.log('playlist end');
            }
        }
    }

    function preloadSounds() {
        soundsReady = 0;
        for(var i = 0; i < sounds.length; i++) {
            sounds[i].audio = document.createElement('audio');
            sounds[i].audio.preload = 'auto';
            sounds[i].audio.oncanplaythrough = getSoundReadyEvent(sounds[i]);
            sounds[i].audio.src = sounds[i].url;
        }
    }

    function playSound(name, callback) {
        var sound = sounds.find(function(sound) {
            return sound.name === name;
        });
        console.log('sound', sound);
        console.log('audio', sound.audio);
        if(sound && sound.loaded) {
            sound.audio.play();
            setTimeout(function() {
                sound.audio.currentTime = 0;
                sound.audio.pause();
                if (typeof callback === 'function') {
                    try {
                        callback();
                    } catch (err) {
                        console.log(err);
                    }
                }
            }, sound.audio.duration * 1000 - 100);
            /*var soundInstance = {
                playing: true,
                audio: sound.audio,
                //timeout: timeout,
                volume: 1,
                setVolume: function(value) {
                    value = Math.min(Math.max(value, 0), 100)/100;
                    this.volume = isNaN(value) ? 1 : value;
                    this.audio.volume = this.volume;
                },
                mute: function() {
                    this.audio.volume = 0;
                },
                unmute: function() {
                  this.audio.volume = this.volume;
                },
                pause: function() {
                    this.playing = false;
                    this.audio.pause();
                },
                play: function() {
                    this.playing = true;
                    this.audio.play();
                    setTimeout(function() {
                        soundInstance.audio.currentTime = 0;
                        soundInstance.audio.pause();
                        if (typeof callback === 'function') {
                            try {
                                callback();
                            } catch (err) {
                                console.log(err);
                            }
                        }
                    }, sound.audio.duration * 1000 - 100);
                }
            };
            return soundInstance;*/
        } else if(!sound) {
            console.log('Sound not found "' + name + '"');
        } else if(!sound.loaded) {
            console.log('Sound not loaded "' + name + '"');
        }
        //return null;
    }

    function getSoundDuration(name) {
        var sound = sounds.find(function(sound) {
            return sound.name === name;
        });
        console.log("sound duration got");
        return sound.audio.duration*1000;
    }

    function playlist(list, index) {
        function isFinished() {
            console.log('isFinished');
            console.log("index: " + index);
            if(++index < list.length) {
                console.log('next sound ' + list[index].name);
                playlist(list, index);
            }
        }
        if (index === undefined) {
            index = 0;
        } else {
            index = Number(index);
            if (isNaN(index)) {
                index = 0;
            }
        }
        console.log("playlist:", list);
        if(list[index].loop !== undefined) {
            console.log(list[index].loop);
            playSoundTime(list[index].name, list[index].loop, function () {
                isFinished();
            });
        } else {
            console.log("no loop");
            playSound(list[index].name, function () {
                isFinished();
            });
        }
    }

    function ladderIncrease() {
        currPrice *= 2;
        console.log(ladderSteps);
        for(var j = ladderSteps.length-1; j > 0; j--) {
            console.log('curr price ' + currPrice);
            console.log('step price ' + ladderSteps[j].innerHTML);
            if(currPrice === 800) {
                currPrice = 500;
            } else if(currPrice === 128000) {
                currPrice = 125000;
            } else if(currPrice === 1000000) {
                currPrice = '1 MILLION';
                console.log('final');
            }

            if((ladderSteps[j].innerHTML === '$'+currPrice)) {
                $('#step'+(ladderSteps.length-j-1)).removeClass('selected');
                $('#step'+(ladderSteps.length-j)).addClass('selected');
                break;
            }
        }
    }

    function onHelpInfo() {


        if(!isInfoBoxShown) {
            jokerEls.addClass('disabled');
            answerTextEls.addClass('disabled');
            answerEls.addClass('disabled');
            answersRadios.prop('disabled', true);

            isInfoBoxShown = true;
            infoBox.show();
            infoBoxCopy.hide();
            var animationName = 'animated bounceInDown';
            var animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

            infoBox.removeClass('animated bounceOutUp');
            infoBox.addClass(animationName).one(animationend, function () {
                infoBox.removeClass(animationName);
            });
        } else {
            jokerEls.removeClass('disabled');
            answerTextEls.removeClass('disabled');
            answerEls.removeClass('disabled');
            answersRadios.prop('disabled', false);

            isInfoBoxShown = false;
            var animationName = 'animated bounceOutUp';
            var animationend = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

            infoBox.addClass(animationName).one(animationend, function () {
                infoBox.removeClass(animationName);
                infoBox.hide();
                infoBoxCopy.show();
            });
        }

    }

    function soundMute() {
        if(!areCreatedSounds) {
            areCreatedSounds = true;
            sounds.map(function (sound) {
                sound.audio = document.createElement('audio');
            });
        }
        sounds.map(function (sound) {
            sound.audio.muted = true;
        });
    }

    function soundUnmute() {
        sounds.map(function (sound) {
            sound.audio.muted = false;
        });
        preloadSounds();
    }

    soundVolumeOnEl.on('click', function (event) {
        console.log('sound off');
        soundMute();
        soundVolumeOnEl.hide();
        soundVolumeOffEl.show();
    });
    soundVolumeOffEl.on('click', function (event) {
        console.log('sound on again');
        soundUnmute();
        soundVolumeOffEl.hide();
        soundVolumeOnEl.show();
    });
   //soundVolumeOffEl.hide();

    answerTextEls.on("click", onLabelClick);

    form.on('submit', onSubmit);
    newGameBtn.on('click', onNewGame);
    infoEl.on('click', onHelpInfo);
    preloadSounds();
    startGame();

})(jQuery);