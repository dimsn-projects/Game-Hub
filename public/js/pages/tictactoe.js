(function($) {
	
	$(document).ready(function(){
		console.log('ready');
		
		function newGame() {
			console.log('newGame disabled=' + newButton.is('[disabled]'));
			if (!newButton.is('[disabled]')) {
				console.log('reset');
				playing = true;
				turn = 0;
				generateGrid();
				newButton.attr('disabled', 'disabled');
				solutions = new Array(gridSize*2+2);
				for (var i = 0, len = solutions.length; i < len; i++) {
					solutions[i] = 0;
				}
				console.log(solutions);
				console.log(solutions.length);
			}
		}
		
		function generateGrid() {
			// ask the user for the grid size
			gridSize = prompt('Enter the size of the grid (3-10)');
			// validate grid size to be btw 3 and 10
			gridSize = Math.min(Math.max(isNaN(gridSize) ? 0 : Number(gridSize), 3), 10);
			console.log('gridSize=' + gridSize);
			board.empty();
			var cell;
			for (var i = 0; i < gridSize * gridSize; i++) {
				cell = $('<div class="cell"><span></span></div>')
					.width(cellSize)
					.height(cellSize)
					.css('margin', cellSpace + 'px 0 0 ' + cellSpace + 'px')
					.data('index', i)
					.on('click', onCellClick);
				cell.find('span').css('font-size', (cellSize - 2) + 'px');
				board.append(cell);
			}
			var boardSize = (cellSize + cellSpace) * gridSize + cellSpace;
			board.width(boardSize);
			board.height(boardSize);
		}
		
		function onCellClick() {
			var cell = $(this),
				index = cell.data('index');
			if (playing && isCellEmpty(cell)) {
				var sign = turn%2 ? 'o' : 'x',
					numSign = sign == 'x' ? 1 : -1;
					
				// add text sign
				cell.find('span').text(sign);
				
				// increment/decrement solutions
				var col = index%gridSize,
					row = (index - col)/gridSize;
				solutions[col] += numSign; 
				solutions[gridSize + row] += numSign;
				if (col == row) {
					solutions[gridSize*2] += numSign;
				}
				if(col + row == gridSize-1) {
					solutions[gridSize*2 + 1] += numSign;
				}
				
				// check for win condition
				var isWin = false,
					winIndex = solutions.indexOf(gridSize);
				if (winIndex != -1) {
					isWin = true;
				} else {
					winIndex = solutions.indexOf(-gridSize);
					if (winIndex != -1) {
						isWin = true;
					}
				}
				if (++turn >= gridSize*gridSize || isWin) {
					playing = false;
					if (isWin) {
						console.log('Player ' + sign.toUpperCase() + ' wins!');
					} else {
						console.log('Nobody wins!');
					}
					console.log('enable button');
					newButton.removeAttr('disabled');
				}
			}
		}
		
		function isCellEmpty(cell) {
			return cell.find('span').text().length == 0;
		}
		
		var playing = false,
			turn = 0,
			gridSize = 0,
			cellSize = 80,
			cellSpace = 2,
			board = $('.board'),
			newButton = $('.new-btn'),
			solutions;
		
		newButton.on('click', newGame);
	});
})(jQuery);
