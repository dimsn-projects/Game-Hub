(function($) {
	
	function startTest() {
		console.log('startTest');
		sendRequest({}, function(data) {
			if (data.question) {
				showQuestion(data.question);
			} else {
				console.log("ERROR: bad data");
			}
		});
	}
	
	function showQuestion(question) {
		questionsHolder.empty();
		
		var props = {
			'question': question.question,
		};
		for (var i = 0, len = question.answers.length; i < len; i++) {
			props['answer' + (i + 1)] = question.answers[i];
		}
		var tpl = tmpl('question', props).appendTo(questionsHolder); 
		
		tpl.find('.answer').each(function(index) {
			$(this).on('click', function() {
				if(!pending){
					pending = true;
					sendRequest({ answer: index }, onAnswerSuccess, onAnswerError);
				}
			});
		});
	}
	function showResult(answers) {
		questionsHolder.empty();
		var tpl = tmpl('result', { answers:answers }).appendTo(questionsHolder); 
		
	}
	
	function onAnswerSuccess(data){
		if(data.question) {
			showQuestion(data.question);
		} else if(data.answers != undefined) {
			showResult(data.answers);
		} else {
			console.log("ERROR: bad data");
		}
		pending = false;
	}
	
	function onAnswerError(){
		pending = false;
	}
	
	function tmpl(tmplName, props) {
		var tmpl = $('#' + tmplName + '_tmpl');
		if (tmpl.size()) {
			tmpl = tmpl.html();
			for (var i = 0, keys = Object.keys(props), len = keys.length; i < len; i++) {
				tmpl = tmpl.replace(new RegExp('{' + keys[i].toUpperCase() + '}', 'g'), props[keys[i]]);
			}
			return $(tmpl);
		} else {
			console.log('ERROR: Template with name "' + tmplName + '" not found!');
		}
		return '';
	}
	
	function sendRequest(props, success, error) {
		$.ajax({
			url: 'wwtbam1.php',
			method: 'POST',
			dataType: 'json',
			data: props,
			success: function(data) {
				console.log(data);
				try {
					success(data);
				} catch (err){}
			},
			error: function() {
				console.log('Connection problem!');	
				try {
					error();
				} catch (err){}
			}
		});
	}
	
	var questionsHolder = $('.questions');
	var pending = false;
	startTest();
})(jQuery);