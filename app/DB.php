<?php

class DB {

	private static $conn;
	private static $noSignValues = ['IS NULL', 'IS NOT NULL'];
	private static $signValues = ['NULL', 'NOW()'];

	public static function connect() {
		if (!isset(self::$conn)) {
			try {
				self::$conn = new mysqli(cfg('DB_SERVER'), cfg('DB_USER'), cfg('DB_USER_PASS'), cfg('DB_NAME'));
				self::$conn->set_charset('utf8');
			} catch (Exception $error) {
				Debug::log($error->getMessage());
				throw new Exception('Unable to connect to the database!');
			}
		}
		return isset(self::$conn);
	}

	public static function connected() {
		if (self::connect()) {
			if (self::$conn->connect_error) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public static function escape($str) {
		if (self::connect()) {
			return mysqli_real_escape_string(self::$conn, $str);
		}
		return $str;
	}

	public static function sanitize($val) {
		if (self::connect()) {
			if (is_string($val)) {
				$val = mysqli_real_escape_string(self::$conn, $val);
			} else if (is_array($val) && Utils::isAssoc($val)) {
				foreach ($val as $k => $v) {
					$val[mysqli_real_escape_string(self::$conn, $k)] = mysqli_real_escape_string(self::$conn, $v);
				}
			}
		}
		return $val;
	}

	public static function error() {
		return self::$conn->error;
	}

	public static function insertId() {
		return self::$conn->insert_id;
	}

	public static function affectedRows() {
		return self::$conn->affected_rows;
	}

	public static function getNumRows($result) {
		return $result !== FALSE ? $result->num_rows : 0;
	}

	public static function query($query) {
		if (self::connect()) {
			Debug::log("query: " . $query);
			return self::$conn->query($query);
		}
		return false;
	}

	public static function select($tableName, $where = null, $columns = null, $options = '') {
	    $result = self::query('SELECT ' . self::formatColumns($columns) . ' FROM `' . $tableName . '`' . self::formatWhere($where). " $options");
	    return $result;
	}

	public static function insert($tableName, $params) {
		$columns = array();
		$values = array();
		foreach ($params as $name => $value) {
			$columns[] = $name;
			$values[] = self::formatValue($value, true);
		}
		return self::query('INSERT INTO `' . $tableName. '` (' . self::formatColumns($columns) . ') VALUES(' . implode(', ', $values) . ')');
	}

	public static function update($tableName, $params, $where) {
		return self::query('UPDATE `' . $tableName . '` SET ' . self::formatParams($params) . self::formatWhere($where));
	}

	public static function delete($tableName, $where) {
		return self::query('DELETE FROM `' . $tableName . '`' . self::formatWhere($where));
	}

	public static function fetch($result, $type = MYSQLI_ASSOC) {
		if (self::getNumRows($result)) {
			$array = array();
			while ($row = mysqli_fetch_array($result, $type)) {
				$array[] = $row;
			}
            return $array;
		}
		return false;
	}

	// PRIVATE METHODS
	private static function formatColumns($columns = null) {
		if ($columns !== null && count($columns)) {
			return '`' . implode('`, `', $columns) . '`';
		}
		return '*';
	}

	private static function formatWhere($where) {
		$result = self::formatParams($where, ' AND ');
		if (strlen($result)) {
			$result = ' WHERE ' . $result;
		}
		return $result;
	}

	private static function formatParams($params, $glue = ', ') {
		$result = [];
		if ($params !== null) {
			foreach ($params as $name => $value) {
				$result[] = '`' . self::sanitize($name) . '`' . self::formatValue($value);
			}
			if (count($result)) {
				return implode($glue, $result);
			}
		}
		return '';
	}

	private static function formatValue($value, $skipSign = false, $noEscape = false) {
		if (is_array($value)) {
			$sign = $skipSign ? '' : (isset($value['sign']) ? $value['sign'] : '=');
			$value = isset($value['value']) ? self::formatValue($value['value'], true, isset($value['noEscape']) ? $value['noEscape'] : false) : '';
		} else {
			$sign = $skipSign ? '' : '=';
			if (!$noEscape) {
				if (!is_numeric($value) && !is_bool($value) && in_array($value, self::$noSignValues)) {
					$sign = '';
					$value = " $value";
				} else {
					if ($value === null) $value = 'NULL';
					if (is_bool($value)) $value = $value ? 1 : 0;
					if (is_numeric($value) || !in_array($value, self::$signValues)) {
						$value = "'" . self::sanitize($value) . "'";
					}
				}
			}
		}
		return $sign . $value;
	}

}