<?php

class Register extends Page
{

    private $username = '';
    private $password = '';
    private $passwordagain = '';
    private $email = '';
    private $question = '';
    private $answer = '';

    function __construct($pageName = 'register')
    {
        parent::__construct($pageName);

    }

    function init($render = false)
    {
        parent::init($render);

        app()->addCssFile('css/forms.css', true);

        if (isset($_POST['username'])) {
            $this->username = $_POST['username'];
            $this->password = $_POST['password'];
            $this->passwordagain = $_POST['passwordagain'];
            $this->email = $_POST['email'];
            $this->question = $_POST['question'];
            $this->answer = $_POST['answer'];

            if (strlen($this->username) < 1) {
                $this->addError("the username is too short");
            }
            if (strlen($this->password) < 6) {
                $this->addError("the password is too short");
            }
            if ($this->passwordagain !== $this->password) {
                $this->addError("the passwords do not match");
            }
            if (strlen($this->email) < 3) {
                $this->addError("the email is too short");
            }
            if (strlen($this->question) < 1) {
                $this->addError("there is no question typed");
            }
            if (strlen($this->answer) < 1) {
                $this->addError("there is no answer typed");
            }
            if ($this->hasErrors() == false) {
                if (DB::connect()) {
                    $result = DB::select(TABLE_USERS, ['email' => $this->email]);
                    if ($result->num_rows == 0) {
                        if (DB::insert(TABLE_USERS, ['username' => $this->username, 'password' => $this->password, 'email' => $this->email, 'question' => $this->question, 'answer' => $this->answer])) {
                            app()->login($this->email, $this->password);
                            app()->createUserStatistics();
                            app()->redirect('/index');
                        } else {
                            $this->addError(DB::error());
                        }
                    } else {
                        $this->email = '';
                        $this->addError("the email is taken");
                    }
                } else {
                    $this->addError("Can't connect to the database");
                }
            }
        }
        $this->render();
    }


}