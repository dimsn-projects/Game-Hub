<?php

class Test extends Page {

    function __construct($pageName = 'test') {
        parent::__construct($pageName);

    }

    function init($render = false) {
        parent::init($render);


        $this->render();
    }

}