<?php
class Logout extends PrivatePage {
	
	function __construct($pageName = 'logout') {
		parent::__construct($pageName);
	}
	
	function init($render = false) {
        parent::init($render);
		app()->logout();
	}
}