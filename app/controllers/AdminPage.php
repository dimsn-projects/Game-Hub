<?php
class AdminPage extends PrivatePage {
	
    function __construct($pageName = null) {
		if (!app()->isAdmin()) {
		    app()->redirect(app()->isLogged() ? '/index' : '/login');
		}
		parent::__construct($pageName);
    }
	
}