<?php

class PrivatePage extends Page {
	
    function __construct($pageName = null) {
		if (!app()->isLogged()) {
		    if ($this->isAjaxRequest()) {
                $this->setResponse(['logout' => true]);
            } else {
                app()->redirect('/login');
            }
		}
		parent::__construct($pageName);
	}
	
}