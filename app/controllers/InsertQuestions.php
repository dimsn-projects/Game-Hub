<?php
class InsertQuestions extends AdminPage {
	
	function __construct($pageName = 'insertQuestions') {
		parent::__construct($pageName);
        app()->addCssFile('css/forms.css');
        app()->addJsFile('js/insertQuestions.js');
    }

    function init($render = false) {
        parent::init($render);
        $this->render();
    }

    function upload($render = false) {
	    parent::init($render);

	    moveUploadedFiles($files);

	    foreach ($files as $key => $file) {
	        try {
                $files[$key]['total'] = $this->textToJson($file['path']);
                $files[$key]['path'] = str_replace(UPLOAD_DIR, '', $file['path']);
            } catch(Exception $error) {
                $files[$key] = [
                    'name' => $file['name'],
                    'error' =>  $error->getMessage()
                ];
            }
        }


	    $this->setResponse($files);
	    $this->render();
    }

    function insert($render = false) {
	    Debug::log('insert');
        parent::init($render);
        if (isset($_POST['file'])) {
            $file = UPLOAD_DIR . $_POST['file'];
            if (is_file($file)) {
                $data = json_decode(file_get_contents($file));
                $startTime = microtime(true);
                while(array_key_exists($data->count, $data->questions) && microtime(true) - $startTime < 0.1) {
                    $question = $data->questions[$data->count];
                    $params = [
                        'question' => $question[0],
                        'answer1' => $question[1][0],
                        'answer2' => $question[1][1],
                        'answer3' => $question[1][2],
                        'answer4' => $question[1][3],
                        'correctAnswer' => $question[2],
                        'difficulty' => $data->count%10 + 1
                    ];
                    if (DB::insert(TABLE_QUESTIONS, $params)) {
                        $data->count++;
                    } else {
                        Debug::log('Questions not added ' . $data->count . ' ' . print_r($params, true));
                    }
                }
                file_put_contents($file, json_encode($data));
                $this->setResponse(['count' => $data->count]);
            } else {
                $this->setResponse(['error' => 'file not found!']);
            }
        } else {
            $this->setResponse(['error' => 'file not set!']);
        }

        $this->render();
    }

    private function textToJson($file) {
        if (!is_file($file)) {
            throw new Exception("File not found");
        }
        $data = [
            'count' => 0,
            'questions' => []
        ];
        $questions = explode("\n\r\n\r", trim(file_get_contents($file)));
        foreach ($questions as $key => $question) {
            $question = explode("\n\r", $question);
            $question[0] = preg_replace('/\s+/',' ', trim(substr($question[0],strpos($question[0], " ")+1)));
            if (count($question) > 1) {
                $question[1] = explode("\n", trim($question[1]));
                foreach ($question[1] as $answerKey => $answer) {
                    $question[1][$answerKey] = trim($answer);
                }
            } else {
                throw new Exception("question $key don't have answers!");
            }
            if (count($question) > 2) {
                $question[2] = array_search(trim($question[2]), $question[1])+1;
            } else {
                throw new Exception("question $key don't have true answer!");
            }
            $data['questions'][] = $question;
        }
        file_put_contents($file, json_encode($data));
        return count($data['questions']);
    }
}