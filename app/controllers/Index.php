<?php
class Index extends PrivatePage {
	
	function __construct($pageName = 'index') {
		parent::__construct($pageName);

	}

    function init($render = false) {
        parent::init($render);
        if(!app()->isLogged()) {
            app()->redirect('/login');
        }
        $this->render();
    }
	
}