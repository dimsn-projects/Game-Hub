<?php
class WwtbamEdit extends AdminPage {

    function __construct($pageName = 'wwtbam-edit') {
        parent::__construct($pageName);

    }

    public function init($render = false) {
        parent::init($render);

        app()->addCssFile('css/dataTable.css', true);
        app()->addCssFile('css/table.css', true);

        $result = DB::select(TABLE_QUESTIONS);
        $this->setData('result', $result);

        $this->render();
    }


}