<?php

class Page {
	
	private $pageName;
	// only logged in users
	private $errors = array();
    private $data = array();
    private $response = array();

	function __construct($pageName = '404') {
		$this->pageName = $pageName;
		app()->setPageName($pageName);

	}
	
    function init($render = true) {
        if(is_file( PUBLIC_DIR . '/css/'.$this->pageName.'.css')) {
            app()->addCssFile('/css/'.$this->pageName.'.css');
        }
        if(is_file(PUBLIC_DIR . '/js/pages/'.$this->pageName.'.js')) {
            app()->addJsFile('/js/pages/'.$this->pageName.'.js');
        }
		if ($render) {
    		$this->render();
    	}
	}

	function hasErrors() {
	    return count($this->errors);
	}

	function getErrors() {
	    return $this->errors;
	}

	function addError($error) {
	    $this->errors[] = $error;
	}

	function getData($name, $def = '') {
	    if (!isset($this->data[$name])) return $def;
	    return $this->data[$name];
	}

	function setData($name, $value) {
	    $this->data[$name] = $value;
	}

	function render() {
        if ($this->isAjaxRequest()) {
            exit(json_encode($this->response));
        }
        include '../partials/layouts/' . app()->getLayout() . '/index.phtml';
	}

	public function getPageName() {
		return $this->pageName;
	}

	public function isAjaxRequest() {
	    return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
    }

    protected function setResponse($response = array()) {
        $this->response = array_merge($this->response, $response);
    }
	
}