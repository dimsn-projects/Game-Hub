<?php

class Users extends AdminPage {
	
	function __construct($pageName = 'users') {
		parent::__construct($pageName);

	}
	
	public function init($render = false) { // second parameter $id removed
		parent::init($render);

        app()->addCssFile('/css/table.css', true);
        app()->addCssFile('/css/dataTables.css', true);
        app()->addCssFile('/css/statisticsTable.css', true);
        app()->addCssFile('/css/test.css', true);
		
		$result = DB::select(TABLE_USERS);
		$this->setData('result', $result);
		
		$this->render();
	}

	public function formatUserLastLogin($lastLogin) {
        $diff = time() - $lastLogin;

        $second = 1;
        $minute = 60;
        $hour = $minute*60;
        $day = $hour*24;

        $type = 'second';
        if ($diff >= $day) {
            $type = 'day';
        } else if ($diff >= $hour) {
            $type = 'hour';
        } else if ($diff >= $minute) {
            $type = 'minute';
        }
        $time = floor($diff/$$type);
        return $time . ' ' . $type . (abs($time) != 1 ? 's' : '') . ' ago';
    }
	
}