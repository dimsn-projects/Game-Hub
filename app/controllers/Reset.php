<?php

class Reset extends Page {
	
	private $stage = 0;
	
	function __construct($pageName = 'reset') {
		parent::__construct($pageName);
		if (isset($_SESSION['stage'])) {
			$stage = $_SESSION['stage'];
			$this->setData('stage', $stage);
		}
	}
	
	
	function init($render = false) {
		parent::init($render);	

		if (isset($_POST['submit'])) {
			if (isset($_POST['email'])) {
                $email = $_POST['email'];
                if ($result = DB::fetch(DB::select(TABLE_USERS, ['email' => $email]))) {
                    $row = $result[0];
                    $question = $row['question'];
                    $this->setData('question', $question);
                    $stage = 1;
                    $this->setData('stage', $stage);
                    $_SESSION['stage'] = 1;
                    $_SESSION['resetUserId'] = $row['id'];
                } else {
                    $this->addError('User with that email address doesnt exist!');
                }
            } else if (isset($_POST['answer'])) {
				$answer = $_POST['answer'];
				if ($result = DB::fetch(DB::select(TABLE_USERS, ['id' => $_SESSION['resetUserId']]))) {
					$row = $result[0];
					if ($answer == $row['answer']) {
						$stage = 2;
						$this->setData('stage', $stage);
						$_SESSION['stage'] = 2;
					} else {
						$this->addError('Wrong answer!');
						$question = $row['question'];
						$this->setData('question', $question);
					}
				} else {
					$this->addError('there is no such an user!');
				}
			} else if (isset($_POST['newpassword']) && isset($_POST['newpasswordagain']) ) {
				if ($_POST['newpassword'] === $_POST['newpasswordagain']) {
					$result = DB::update(TABLE_USERS, ['password' => $_POST['newpassword']], ['id' => $_SESSION['resetUserId']]);
					unset($_SESSION['stage']);
					unset($_SESSION['resetUserId']);
					app()->redirect('/index');
				} else {
					$this->addError('Different passwords');
				}
			} else {
				$this->addError('There are no passwords typed!');
			}
		} else {
			$_SESSION['stage'] = $stage = 0;
			$this->setData('stage', $stage);
		}
		
		$this->render();
	}

		
}