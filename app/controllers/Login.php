<?php

class Login extends Page {

	function __construct($pageName = 'login') {
		parent::__construct($pageName);

	}

    function init($render = false) {
        parent::init($render);
        app()->addCssFile('css/forms.css', true);

		if (app()->isLogged()) {
			app()->redirect('/index');
		} else if (isset($_POST['email'])) {
			$email = $_POST['email'];
			$password = $_POST['password'];
            $this->setData('email', $email);
			try {
				app()->login($email, $password);
			} catch (Exception $error) {
				$this->addError($error->getMessage());
			}
        }
		
        $this->render();
    }

}