<?php

class WwtbamQuestionNew extends AdminPage {
	
	function __construct($pageName = 'wwtbam-question-new') {
		parent::__construct($pageName);
		$this->setData('created', false);
	}
	
	public function init($render = false) {
		parent::init($render);
		
		if(isset($_POST['submit'])) {
			$question = $_POST['question'];
			$difficulty = $_POST['difficulty'];
			$answer1 = $_POST['answer1'];
			$answer2 = $_POST['answer2'];
			$answer3 = $_POST['answer3'];
			$answer4 = $_POST['answer4'];
			$answer = $_POST['answer'];
			
			if (trim(strlen($question)) < 1) {
				$this->addError('the question is too short');
			}
			if (trim(strlen($answer1)) < 1) {
				$this->addError('answer1 is too short');
			}
			if (trim(strlen($answer2)) < 1) {
				$this->addError('answer2 is too short');
			}
			if (trim(strlen($answer3)) < 1) {
				$this->addError('answer3 is too short');
			}
			if (trim(strlen($answer4)) < 1) {
				$this->addError('answer4 is too short');
			} 
			
			if (trim(strlen($answer)) < 1) {
				$this->addError('the correct answer is too short');
			} else if (!isInteger($answer)) {
				$this->addError('the correct answer is not a number');
			} else if ($answer < 1 || $answer > 10) {
				$this->addError('the correct answer number is out of the range');
			} 
			
			if (trim(strlen($difficulty)) < 1) {
				$this->addError('the difficulty is undefined');
			} else if (!isInteger($difficulty)) {
				$this->addError('the difficulty is not a number');
			} else if ($difficulty < 1 || $difficulty > cfg('maxQuestions')) {
				$this->addError('the difficulty is out of the range');
			}
			if ($this->hasErrors() == false) {
				$params = array(
					'question' => $question,
					'difficulty' => $difficulty,
					'answer1' => $answer1,
					'answer2' => $answer2,
					'answer3' => $answer3,
					'answer4' => $answer4,
					'correctAnswer' => $answer,
				);
				
				if (DB::insert(TABLE_QUESTIONS, $params) === TRUE) {
					app()->redirect('/wwtbam-edit');
				} else {
					 $this->addError('Error creating record: ' . DB::error());
				}
			}
		}
		
		$this->render();
	}
	
}