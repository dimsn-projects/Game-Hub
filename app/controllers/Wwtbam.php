<?php
class Wwtbam extends PrivatePage {

	public $maxQuestions = 14,
            $timeTotal = 1000,
	        $timeExtra = 1;
	private $playing = false,
            $game,
            $question,
            $correctAnswer,
            $questionStruct,
            $questionStructProps = ['question', 'answer1', 'answer2', 'answer3', 'answer4', 'correctAnswer'];
    public $goup = false;

	function __construct($pageName = 'wwtbam') {
		parent::__construct($pageName);

	}

    public function init($render = false) {
        app()->addCssFile('/css/specialTimer.css');
        app()->addCssFile('/css/animate.css');
        app()->addJsFile('/js/specialTimer.js', true);

        //Debug::log('wttbam init');
        parent::init($render);
        //have we finished the quiz
        if (!$this->getCurrentGame()) {
            //Debug::log('create new game');
            $this->createNewGame();
        }
        //we get to the questions anyway
        if ($this->getGameQuestion()) {
            // check if a joker is selected
            if(isset($_POST['type'])) {
                Debug::log('type of joker: ' . $_POST['type']);
                $jokerName = $_POST['type'];
                if ($jokerName == 'cut') {
                    $jokerName = 'joker3used';
                } else if ($jokerName == 'fifty-fifty') {
                    $jokerName = 'joker2used';
                } else if ($jokerName == 'time') {
                    $jokerName = 'joker1used';
                }
                $params = [
                    $jokerName => true
                ];
                $where = [
                    'gameId' => $this->getGameId(),
                    'questionId' => $this->getQuestionId()
                ];

                DB::update(TABLE_GAMES_QUESTIONS, $params, $where);

                if ($this->isAjaxRequest()) {
                    $this->setResponse(['used' => true, 'type' => $_POST['type']]);
                }
            } else if ($this->isAjaxRequest()) {
                Debug::log('HERE');

                $result8 = DB::select(TABLE_GAMES_QUESTIONS, ['gameId' => $this->getGameId(), /*'questionId' => $this->getQuestionId(),*/ 'joker1used' => 1]);
                $result9 = DB::select(TABLE_GAMES_QUESTIONS, ['gameId' => $this->getGameId(), /*'questionId' => $this->getQuestionId(),*/ 'joker2used' => 1]);
                $result10 = DB::select(TABLE_GAMES_QUESTIONS, ['gameId' => $this->getGameId(), /*'questionId' => $this->getQuestionId(),*/ 'joker3used' => 1]);

                /*Debug::log(serialize($result8));
                Debug::log(serialize($result9));
                Debug::log(serialize($result10));*/

                $where = [
                    'gameId' => $this->getGameId(),
                    'questionId' => $this->getQuestionId()
                ];

                $returnValues = [
                    'time' => 0,
                    'fifty-fifty' => 0,
                    'cut' => 0
                ];

                if($rows = DB::fetch($result8)) {
                    $params = [
                        'joker1used' => true
                    ];
                    DB::update(TABLE_GAMES_QUESTIONS, $params, $where);
                   // if ($this->isAjaxRequest()) {
                       // $this->setResponse(['used' => true, 'type' => 'time']);
                   // }
                    $returnValues['time'] = 1;
                }
                if($rows = DB::fetch($result9)) {
                    $params = [
                        'joker2used' => true
                    ];
                    DB::update(TABLE_GAMES_QUESTIONS, $params, $where);
                   //  if ($this->isAjaxRequest()) {
                       // $this->setResponse(['used' => true, 'type' => 'fifty-fifty']);
                   //  }
                    $returnValues['fifty-fifty'] = 1;
                }
                if($rows = DB::fetch($result10)) {
                    $params = [
                        'joker3used' => true
                    ];
                    DB::update(TABLE_GAMES_QUESTIONS, $params, $where);
                    // if ($this->isAjaxRequest()) {
                      // $this->setResponse(['used' => true, 'type' => 'cut']);
                    // }
                    $returnValues['cut'] = 1;
                }
                $this->setResponse(['time' => $returnValues['time'], 'fifty' => $returnValues['fifty-fifty'], 'cut' => $returnValues['cut']]);
            }
            // checking for losses/wins
            if (time() - $this->getTimeStarted() <= $this->timeTotal + $this->timeExtra) {
                Debug::log('time is not up');
                if (isset($_POST['answer'])) {
                    if ($_POST['answer'] !== -1) {
                        $userAnswer = $_POST['answer'];
                        $params = [
                            'answer' => $userAnswer,
                            'timeFinished' => 'NOW()'
                        ];
                        $where = [
                            'gameId' => $this->getGameId(),
                            'questionId' => $this->getQuestionId()
                        ];
                        if (DB::update( TABLE_GAMES_QUESTIONS, $params, $where)) {
                            $trueAnswer = $this->getQuestionAnswer();
                            $isRightAnswer = $userAnswer == $trueAnswer;
                            if ($isRightAnswer) {
                                Debug::log('right answer');
                                if (!$this->areAllQuestionsAnswered()) {
                                    $this->addGamesQuestion();
                                } else {
                                    $this->stopCurrentGame();
                                }
                            } else {
                                Debug::log('wrong answer');
                                if ($this->isAjaxRequest()) {
                                    $this->setResponse(['trueAnswer' => $trueAnswer]);
                                    $this->goup = true;
                                }
                                $this->stopCurrentGame();
                            }
                        } else {
                            $this->addError("Update error!");
                        }
                    } else {
                        Debug::log('answer is -1');
                        $this->stopCurrentGame();
                    }
                } else {
                    Debug::log('get current question');
                    $result3 = DB::select(TABLE_QUESTIONS, ['id' => $this->getQuestionId()], null, 'ORDER BY RAND() LIMIT 0,1');
                    if ($rows = DB::fetch($result3)) {
                        $this->questionStruct = $rows[0];
                        //Debug::log("New QUESTION");
                        //print_r($this->questionStruct);
                    } else {
                        Debug::log('No questions');
                    }
                }
            } else {
                Debug::log('time expired');
                if ($this->isAjaxRequest()) {
                    $this->setResponse(['timeUp' => true]);
                }
                $this->stopCurrentGame();
            }
        } else {
            Debug::log('new game');
            $this->addGamesQuestion();
        }

        $this->render();
    }

    function newGame() {
	    Debug::log("newGame");
        parent::init(false);

	    // check for already started game and stop it
        if (!$this->getCurrentGame()) {
            $this->createNewGame();
            $this->addGamesQuestion();
        } else {
            $this->setResponse(['error' => 'There is already started game!']);
        }

        $this->render();
    }

    function timeUp() {
        parent::init(false);

        if ($this->getCurrentGame()) {
            $this->stopCurrentGame();
        } else {
            $this->setResponse(['error' => 'There is not started game!']);
        }

        $this->render();
    }

    public function stopCurrentGame() {
        $this->playing = false;
	    $params = array(
	        'ended' => 1
        );
	    $where = array(
	        'id' => $this->getGameId()
        );
	    DB::update(TABLE_GAMES, $params, $where);
	    if ($this->isAjaxRequest()) {
            $this->setResponse(['ended' => true]);
        }
    }

    public function isFinish() {
        return !$this->playing;
	}

	public function getQuestion() {
        return $this->questionStruct;
    }

    public function getGameId() {
        return $this->game['id'];
    }

    public function getQuestionId() {
	    return $this->question['questionId'];
    }

    public function getQuestionDifficulty() {
        $result = DB::select(TABLE_QUESTIONS, ['id' => $this->getQuestionId()], ['difficulty']);
        if ($rows = DB::fetch($result)) {
            return $rows[0]['difficulty'];
        }
	    return false;
    }

    public function getQuestionAnswer() {
	    if (!$this->correctAnswer) {
            $result = DB::select(TABLE_QUESTIONS, ['id' => $this->getQuestionId()], ['correctAnswer']);
            if ($rows = DB::fetch($result)) {
                $this->correctAnswer = $rows[0]['correctAnswer'];
            }
        }
        return $this->correctAnswer;
    }

    public function getRightAnswers() {
        if (isset($_POST['answer'])) {
            //$_POST['answer'] == ($this->getQuestionAnswer()) ? $this->maxQuestions :
            return $this->getQuestionsCount() - 1;
        } else {
            return $this->getQuestionsCount();
        }
    }


    public function getTimeStarted() {
        $result = DB::select(TABLE_GAMES_QUESTIONS, ['gameId' => $this->getGameId(), 'questionId' => $this->getQuestionId()], ['timeStarted']);
        if ($rows = DB::fetch($result)) {
            return strtotime($rows[0]['timeStarted']);
        } else {
            Debug::log('error in getTimeStarted');
        }
        return 0;
    }

    // PRIVATE
    /*private function updateUserStatistics($name, $value) {
        $params = array(
            $name => $value
        );
        $where = array(
            'id' => app()->getCurrentUserId()
        );
        if (!DB::update(TABLE_USERS_STATISTICS, $params, $where)) {
            $this->addError('Unable to update user statistics!');
        }
    }

    private function getUserStatistics($update = false) {
	    if (!$this->usersStatistics || $update) {
	        $result = DB::fetch("SELECT * FROM `" . TABLE_USERS_STATISTICS . "` WHERE `userId` = " . app()->getCurrentUserId());
	        if ($result) {
                $this->usersStatistics = $result[0];
            } else {
	            Debug::log('Unable to get user statistics for user with id ' . app()->getCurrentUserId());
            }
        }
	    return $this->usersStatistics;
    }*/

    private function getCurrentGame() {
        if (!$this->game) {
            $result = DB::fetch(DB::select(TABLE_GAMES, ['ended' => 0, 'userId' => app()->getCurrentUserId()]));
            //Debug::log("fetch: " . json_encode($result));
            if ($result) {
                $this->playing = true;
                $this->game =  $result[0];
            }
        }
        return $this->game;
    }

    private function getGameQuestion() {
        if (!$this->question) {
            $result = DB::fetch(DB::select(TABLE_GAMES_QUESTIONS, ['gameId' => $this->getGameId(), 'answer' => 0 ]));
            if ($result) {
                $this->question = $result[0];
            }
        }
        return $this->question;
    }

    private function getQuestionsCount() {
        $result = DB::fetch(DB::query("SELECT COUNT(*) as `count` FROM `". TABLE_GAMES_QUESTIONS ."` WHERE `gameId` = '{$this->getGameId()}' AND `answer` != '0' "));
        return $result[0]['count'];
    }

    private function areAllQuestionsAnswered() {
        return ($this->getQuestionsCount() == $this->maxQuestions);
    }

    private function createNewGame() {
        $params = array(
            'userId' => app()->getCurrentUserId()
        );
        DB::insert(TABLE_GAMES, $params);
        $result = DB::fetch(DB::select(TABLE_GAMES , ['ended' => 0, 'userId' => app()->getCurrentUserId()]));
        //Debug::log("create new game: ". json_encode($result));
        $this->game = null;
        if ($result !== false) {
            $this->playing = true;
            $this->game = $result[0];
        }
    }

    private function addGamesQuestion() {
	    $difficulty = 1;
        if ($this->question) {
            $difficulty = $this->getQuestionDifficulty() + 1;
        }
        $result = DB::select(TABLE_QUESTIONS, ['difficulty' => $difficulty], null, 'ORDER BY RAND() LIMIT 0,1');
        $this->questionStruct = $result->fetch_assoc();
        if ($this->isAjaxRequest()) {
            Debug::log("Next Question");
            $this->setResponse(['nextQuestion' => app()->getProps($this->questionStructProps, $this->questionStruct)]);
        }

        $params = array(
            'gameId' => $this->getGameId(),
            'questionId' => $this->questionStruct['id']
        );
        DB::insert(TABLE_GAMES_QUESTIONS, $params);
        $result2 = DB::fetch(DB::select(TABLE_GAMES_QUESTIONS, ['gameId' => $this->getGameId(), 'answer' => 0]));
        $this->question = $result2[0];
    }

    // JOKERS FUNCTION
    private function fiftyFifty() {
        //$answers = DB::fetch(DB::select(TABLE_QUESTIONS, ['id' => $this->getQuestionId()], ['answer1','answer2','answer3','answer4','correctAnswer']));
        if ($this->isAjaxRequest()) {
            $this->setResponse(['trueAnswer' => $this->getQuestionAnswer()]);
        } else {
            Debug::log("It's not ajax request");
        }
    }
    public function initJoker($type) {

    }
}