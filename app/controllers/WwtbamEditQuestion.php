<?php
class WwtbamEditQuestion extends AdminPage {
	
	private $id;
	
	function __construct($pageName = 'wwtbam-edit-question') {
		parent::__construct($pageName);

	}
	
	public function init($render = false) {
		parent::init($render);

		$result = DB::select(TABLE_QUESTIONS, ['id' => $this->id]);
		//Debug::log("id: " . $this->id);
		//Debug::log(print_r($result, true));
		if ($result !== false && $result->num_rows == 1) {
			
			$questionRow = $result->fetch_assoc();
			$this->setData('questionRow', $questionRow);
			
			if (isset($_POST['submit'])) {
				$question = $_POST['question'];
				$difficulty = $_POST['difficulty'];
				$answer1 = $_POST['answer1'];
				$answer2 = $_POST['answer2'];
				$answer3 = $_POST['answer3'];
				$answer4 = $_POST['answer4'];
				$answer = $_POST['answer'];
				
				if (trim(strlen($question)) < 1) {
					$this->addError('the question is too short');
				}
				if (trim(strlen($answer1)) < 1) {
					$this->addError('answer1 is too short');
				}
				if (trim(strlen($answer2)) < 1) {
					$this->addError('answer2 is too short');
				}
				if (trim(strlen($answer3)) < 1) {
					$this->addError('answer3 is too short');
				}
				if (trim(strlen($answer4)) < 1) {
					$this->addError('answer4 is too short');
				}
				
				if (trim(strlen($answer)) < 1) {
					$this->addError('The correct answer is too short!');
				} else if (!isInteger($answer)) {
					$this->addError('The correct answer is not a number!');
				} else if ($answer < 1 || $answer > 4) {
					$this->addError('The correct answer number is out of range!');
				}
				
				if (trim(strlen($difficulty)) < 1) {
					$this->addError('the difficulty is undefined');
				} else if (!isInteger($difficulty)) {
					$this->addError('the difficulty is not a number');
				} else if ($difficulty < 1 || $difficulty > cfg('maxQuestions')) {
					$this->addError('the difficulty is out of the range');
				}
				
				if (!$this->hasErrors()) {
					$params = array(
						'question' => $question,
						'difficulty' => $difficulty,
						'answer1' => $answer1,
						'answer2' => $answer2,
						'answer3' => $answer3,
						'answer4' => $answer4,
						'correctAnswer' => $answer
					);
					$where = array(
						'id' => $this->id
					);
					if (DB::update('questions', $params, $where) === TRUE) {
						app()->redirect('/wwtbam-edit');
					} else {
						Debug::log('Error updating question: ' . DB::error());
						$this->addError('Error updating question!');
					}
				}
			}
		} else {
			$this->addError('Unable to find question');
		} 
		
		$this->render();
	}
	
	function initEditQuestion($id) {
		$this->id = $id;
		$this->init();
	}

}