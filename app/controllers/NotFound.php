<?php
class NotFound extends Page {
	
	function __construct($pageName = 'not-found') {
		parent::__construct($pageName);

	}

    function init($render = false) {
        parent::init($render);

        app()->addCssFile('css/pages/index.css');

        $this->render();
    }
	
}