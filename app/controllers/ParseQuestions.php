<?php

class ParseQuestions extends AdminPage {

    const QUESTION = 'question';
    const QUESTION_ADDED = 'questionAdded';
    const ANSWERS = 'answers';
    const RIGHT_ANSWER = 'rightAnswer';

    private $questions = [];
    private $type = self::QUESTION;

    function __construct($pageName = 'parse-questions') {
        parent::__construct($pageName);
    }

    function init($render = false) {
        parent::init($render);

        $file_handle = fopen(PROJECT_DIR . '/questions.txt',"r");
        while (!feof($file_handle)) {
            $this->parseLine(fgets($file_handle));
        }
        fclose($file_handle);

        $this->render();
    }

    private function parseLine($line) {
        switch ($this->type) {
            case self::QUESTION:
                $line = trim($line);
                if (strlen($line)) {
                    $this->addQuestion(substr($line, strpos($line, ' ') + 1));
                    $this->type = self::QUESTION_ADDED;
                }
                break;
            case self::QUESTION_ADDED:
                $line = trim($line);
                if (strlen($line)) {
                    $this->addQuestion($line, true);
                } else {
                    $this->type = self::ANSWERS;
                }
                break;
            case self::ANSWERS:
                $line = trim($line);
                if (strlen($line)) {
                    $this->addAnswer($line);
                } else {
                    $this->type = self::RIGHT_ANSWER;
                }
                break;
            case self::RIGHT_ANSWER:
                $line = trim($line);
                if (strlen($line)) {
                    $this->addRightAnswer($line);
                } else {
                    $this->type = self::QUESTION;
                }
                break;
        }
    }

    private function addQuestion($question, $sameQuestion = false) {
        if (!$sameQuestion) {
            $this->questions[] = [
                'question' => $question,
                'answers' => [],
                'rightAnswer' => ''
            ];
        } else {
            $questionObj =& $this->getCurrentQuestion();
            $questionObj['question'] .= ' ' . $question;
        }
    }

    private function addAnswer($answer) {
        $questionObj =& $this->getCurrentQuestion();
        $questionObj['answers'][] = $answer;
    }

    private function addRightAnswer($rightAnswer) {
        $questionObj =& $this->getCurrentQuestion();
        $questionObj['rightAnswer'] = $rightAnswer;
    }

    private function & getCurrentQuestion() {
        return $this->questions[count($this->questions) - 1];
    }

    public function getQuestions() {
        return $this->questions;
    }

}