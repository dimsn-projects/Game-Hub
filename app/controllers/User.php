<?php

class User extends PrivatePage {
	
	private $id;
		
	function __construct($pageName = 'user') {
		parent::__construct($pageName);
	}
	
	public function init($render = false) {
		parent::init($render);
		
		if ($this->id === null) {
			$this->id = app()->getCurrentUserId();
		}
		$result = DB::select(TABLE_USERS, ['id' => $this->id]);
		$this->setData('result', $result);
		if ($result->num_rows == 1) {
			$user = $result->fetch_assoc();
			$this->setData('user', $user);
			if (isset($_POST['submit'])) {
				if ($_POST['password'] === $user['password']) {
					
					$username = $_POST['username'];
					$password = $_POST['newpassword'];
					$passwordagain = $_POST['newpasswordagain'];
					$email = $_POST['email'];
					$question = $_POST['question'];
					$answer = $_POST['answer'];
					$updatePass = false;
					
					if (trim(strlen($username)) < 1) {
						$this->addError('the username is too short');
					}
					if (trim(strlen($password)) != 0) {
						if (trim(strlen($password)) < 6) {
							$this->addError('the password is too short');
						} else if ($passwordagain !== $password) {
							$this->addError('the passwords do not match');
						} else {
							$updatePass = true;
						}
					}
					if (trim(strlen($email)) < 3) {
						$this->addError('the email is too short');
					}
					if (trim(strlen($question)) < 1) {
						$this->addError('there is no question typed');
					}
					if (trim(strlen($answer) < 1)) {
						$this->addError('there is no answer typed');
					}
					if (!$this->hasErrors()) {
						$params = [
						    'email' => $email,
                            'username' => $username,
                            'question' => $question,
                            'answer' => $answer
                        ];
                        if ($updatePass) {
                            $params['password'] = $password;
                        }
						if (DB::update(TABLE_USERS, $params, ['id' => $this->id])) {
							app()->updateUserSession(array(
								'email' => $email,
								'username' => $username
							));
							app()->redirect(app()->isAdmin() ? '/users' : '/index');
						} else {
							$this->addError('Error updating record: ' . DB::error());
						}
					}
				} else {
					$this->addError('Wrong password!');
				}
			}
		} else {
			$this->addError('Unable to find user');
		} 


		$this->render();
	}
	
	function initUser($id = null) {
		$this->id = $id !== null && !app()->isAdmin() ? null : $id;
		$this->init();
	}
	
}