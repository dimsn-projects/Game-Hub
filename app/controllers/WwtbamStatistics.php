<?php

class WwtbamStatistics extends PrivatePage {

	public $maxQuestions = 10;

	function __construct($pageName = 'wwtbam-statistics') {
		parent::__construct($pageName);

	}

    public function init($render = false) {
        parent::init($render);

        app()->addCssFile('css/table.css');
        app()->addCssFile('css/dataTables.css');
        app()->addCssFile('css/statisticsTable.css');

        $this->render();
    }

    public function getUsersStatistics() {
        return DB::fetch(DB::query("SELECT
          	DISTINCT `" . TABLE_USERS . "`.`username`,
            COUNT(`" . TABLE_USERS . "`.`id`) as `played`,
            COALESCE(`games-won`.`won`, 0) as `won`
            FROM `" . TABLE_USERS . "`
            LEFT JOIN `" . TABLE_GAMES . "` ON `" . TABLE_GAMES . "`.`userId` = `" . TABLE_USERS . "`.`id`
            LEFT JOIN (SELECT
                  `u`.`id`,
                  COUNT(`u`.`id`) as `won`
                FROM `" . TABLE_USERS . "` as `u`
                LEFT JOIN `" . TABLE_GAMES . "` as `g` ON `g`.`userId` = `u`.`id`
                LEFT JOIN `". TABLE_GAMES_QUESTIONS . "` ON `" . TABLE_GAMES_QUESTIONS . "`.`gameId` = `g`.`id`
                LEFT JOIN `" . TABLE_QUESTIONS . "` ON `" . TABLE_QUESTIONS . "`.`id` = `" . TABLE_GAMES_QUESTIONS . "`.`questionId`
                WHERE
                    `" . TABLE_GAMES_QUESTIONS . "`.`gameId` = `g`.`id`
                    AND `" . TABLE_QUESTIONS . "`.`difficulty` = 10
                GROUP BY `u`.`id`
            ) as `games-won` ON `games-won`.`id` = `" . TABLE_USERS . "`.`id`
            WHERE
                `" . TABLE_GAMES . "`.`ended` = '1'
            GROUP BY `" . TABLE_USERS . "`.`id`
        "));
    }

}