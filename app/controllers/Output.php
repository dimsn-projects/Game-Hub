<?php
class Output extends Page {

    public $output;
	function __construct($pageName = 'output') {
		parent::__construct($pageName);

	}

    function init($render = false) {
        parent::init($render);

        if (isset($_POST['answer'])) {
            echo(json_encode($_POST['answer']));
            app()->redirect('wwtbam');
        }
        $this->render();
    }

}