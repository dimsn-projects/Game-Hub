<?php

class Topka extends PrivatePage {
    function __construct($pageName = 'topka') {
        parent::__construct($pageName);

    }

    function setPosition() {
        if ($this->isAjaxRequest()) {
            if (isset($_POST['x']) && isset($_POST['y'])) {
                $params = [
                    'x' => $_POST['x'],
                    'y' => $_POST['y']
                ];
                file_put_contents(PROJECT_DIR . '/topka.json', json_encode($params));
            }
        } else {
            exit('bad request!');
        }
    }
    function getPosition() {
        if ($this->isAjaxRequest()) {
            $data = json_decode(file_get_contents(PROJECT_DIR . '/topka.json'), true);
            $this->setResponse($data);
            $this->render();
        }
    }

}