<?php

class CGI {	
	
	private $prepare = false;

	function CGI($skip = array()) {
		// form elements that don't need character replacing
		// are defined in $skip. such elements are passwords.
		foreach($_POST as $name=>$value) {
			if ($this->prepare) {
				$flag = 0;
				foreach($skip as $var) if ($var == $name) $flag = 1;

				if (is_array($value)) {
					foreach ($value as $n=>$v) $value[$n] = ($flag) ? addslashes($v) : $this->prepare($v);
				} else $value = ($flag) ? addslashes($value) : $this->prepare($value);
			}
			$this->POST($name, $value);
		}

		foreach($_GET as $name=>$value) {
			if ($this->prepare) {
				$flag = 0;
				foreach($skip as $var) if ($var == $name) $flag = 1;

				if (is_array($value)) {
					foreach ($value as $n=>$v) $value[$n] = ($flag) ? addslashes($v) : $this->prepare($v);
				} else $value = ($flag) ? addslashes($value) : $this->prepare($value);
			}
			$this->GET($name, $value);
		}
	}
	
	public static function prepare($text) {
		// replace some characters with their HTML code
		// otherwise we may end up even with a script execution
		return trim(addslashes(str_replace(array("&", "\"", "<", ">"), array("&amp;", "&quot;", "&lt;", "&gt;"), $text)));
		//return trim(addslashes(str_replace(array("&", "\"", "'", "<", ">"), array("&amp;", "&quot;", "&apos;", "&lt;", "&gt;"), $text)));
		//return addslashes(htmlspecialchars($text));
	} 

	// since we're going to use the methods statically
	// a special "static" variable is defined to contain the form elements
	// get: METHOD("name")
	// set: METHOD("name", "value")
	public static function POST($name = null, $value = null, $update = false) {
		static $__post;
		if (isset($name) && isset($value)) {
			$__post[$name] = $value;
			if ($update) $_POST[$name] = $value;
		}
		elseif ($name) return isset($__post[$name]) ? $__post[$name] : null;
		else return $_POST;
	}
	
	public static function GET($name = null, $value = null, $update = false) {
		static $__get;
		if (isset($name) && isset($value)) {
			$__get[$name] = $value;
			if ($update) $_GET[$name] = $value;
		}
		elseif ($name) return isset($__get[$name]) ? $__get[$name] : null;
		else return $_GET;
	}
	
	public static function isSetPOST($prop) {
		return isset($_POST[$prop]);
	}
	
	public static function isSetGET($prop) {
		return isset($_GET[$prop]);
	}
	
	public static function serialize($what = 'post') {
		$res = array();
		switch (strtolower($what)) {
			case 'post':
				foreach($_POST as $k => $v) $res[$k] = $v;
				break;
			case 'get':
				foreach($_GET as $k => $v) $res[$k] = $v;
				break;
		}
		
		return serialize($res);
	}
	
	public static function unsetGET($prop) {
		unset($_GET[$prop]);
	}
	
	public static function unsetPOST($prop) {
		unset($_POST[$prop]);
	}

}