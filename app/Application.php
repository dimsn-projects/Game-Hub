<?php
session_start();
include_once __DIR__ . '/cfg.php';
include_once __DIR__ . '/Debug.php';
include_once __DIR__ . '/utils.php';

include __DIR__ . '/../vendor/autoload.php';

use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;

class Application {

	private $cssFiles = array();
	private $jsFiles = array();
	private $pageName = '';
	private $controller;

	static function __autoload($className) {
	    if(is_file($class =__DIR__ . "/$className.php")) { // base classes
            app()->require_file($class);
        } else if(is_file($class =__DIR__ . "/controllers/$className.php")) { // controllers
            app()->require_file($class);
        } else {
	        Debug::log("can't find class");
        }
    }

	function __construct() {
	    spl_autoload_register([get_class($this), '__autoload']);
        define('PROJECT_DIR', realpath(__DIR__ . '/..'));
        define('UPLOAD_DIR', realpath(PROJECT_DIR . '/uploads'));
        define('PUBLIC_DIR', realpath(PROJECT_DIR . '/public'));

        //cfg('DB_SERVER', 'localhost');
        cfg('DB_SERVER', '127.0.0.1');
        cfg('DB_NAME', 'gamehub');
        cfg('DB_USER', 'gamehub');
        cfg('DB_USER_PASS', 'sUCbSPY36vm9RpNS');

        // database tables
        define('TABLE_USERS_STATISTICS', 'users-statistics');
        define('TABLE_GAMES_QUESTIONS', 'games-questions');
        define('TABLE_USERS', 'users');
        define('TABLE_QUESTIONS', 'questions');
        define('TABLE_GAMES', 'games');
        define('TABLE_TICTACTOE_GAMES', 'tictactoe-games');

        // questions
        cfg('maxQuestions', 10);
        cfg('maxAnswers', 4);

        cfg('pageTitle', 'Gamehub');
        cfg('layout', 'default');

    }

	public function run() {
        $router = new RouteCollector();
		$router->any('/', function() { app()->init('Index'); });
		$router->any('/index', function() { app()->init('Index'); });
		$router->any('/login', function() { app()->init('Login'); });
		$router->any('/test-hotspot', function() { app()->init('Hotspot'); });
		$router->any('/logout', function() { app()->init('Logout'); });
		$router->any('/register', function() { app()->init('Register'); });
		$router->any('/reset', function() { app()->init('Reset'); });
		$router->any('/puzzle', function() { app()->init('Puzzle'); });
		$router->any('/tictactoe', function() {app()->init('Tictactoe'); });
        $router->any('/wwtbam', function() {app()->init('Wwtbam'); });
        $router->get('/wwtbam/new', function() { app()->init('Wwtbam', 'newGame'); });
        $router->get('/wwtbam/timeup', function() { app()->init('Wwtbam', 'timeUp'); });
        $router->get('/wwtbam/joker/{type:[%\w\-]+}', function($type) { app()->init('Wwtbam', 'initJoker', $type); });
        $router->get('/test', function() { app()->init('Test'); });
        $router->get('/topka', function() { app()->init('Topka'); });
        $router->post('/topka/position', function() { app()->init('Topka', 'setPosition'); });
        $router->get('/topka/position', function() { app()->init('Topka', 'getPosition'); });
        //tmp
        $router->get('/insert', function() { app()->init('InsertQuestions'); });
        $router->post('/insert/upload', function() { app()->init('InsertQuestions', 'upload'); });
        $router->post('/insert/questions', function() { app()->init('InsertQuestions', 'insert'); });
        // admin
		$router->any('/wwtbam-edit', function() { app()->init('WwtbamEdit'); });
		$router->get('/wwtbam-edit/{id:\d+}', function($id) {app()->init('WwtbamEditQuestion', 'initEditQuestion', $id); });
		$router->any('/wwtbam-edit/new', function() { app()->init('WwtbamQuestionNew'); });
		$router->any('/wwtbam-statistics', function() { app()->init('WwtbamStatistics'); });
		$router->any('/users', function() { app()->init('Users'); });
		$router->any('/user/{id:\d+}', function($id) { app()->init('User', 'initUser', $id); });
		$router->any('/user', function() { app()->init('User'); });
		//$router->any('/parse-questions', function() { app()->init('Wwtbam'); });
		$router->any('/output', function() { app()->init('Output'); });
		// 404
		$router->any('/{page}', function($page) { app()->init('NotFound'); });

		$dispatcher = new Dispatcher($router->getData());
		echo $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
	}
	public function init($class, $method = 'init', ...$params) {
        if ($method === null) {
            $method = Utils::getDefaultArgumentValue(new ReflectionMethod(static::class, __FUNCTION__), 'method');
        }
        if (count($params)) {
            //$controller = new $class;
            //$this->setController($controller);
            //$controller->$method(...$params)
            $this->setController(new $class)->$method(...$params);
        } else {
            $this->setController(new $class)->$method();
        }
    }

	public function setController($controller) {
	    if (!$this->controller) {
	        $this->controller = $controller;
	    }
        return $this->controller;
	}

    public function getController() {
        return $this->controller;
    }

    function redirect($path) {
        header("Location: $path");
        exit();
    }

	function isAdmin() {
    	if ($this->isLogged()) {
    		$id = $_SESSION['user']['id'];
    		$result = DB::select(TABLE_USERS, ['id' => $id]);
    		if ($result !== false && $result->num_rows > 0) {
    			$row = $result->fetch_assoc();
    			if ($row['isAdmin'] == 1) {
    				return true;
    			}
    		}
    	}
    	return false;
    }

    function isLogged() {
        return isset($_SESSION['user']);
    }

	function logout() {
		unset($_SESSION['user']);
		$this->redirect('index');
	}

	function login($email, $password) {
		if (DB::connect()) {
		    //Debug::log("loged");
			$result = DB::select(TABLE_USERS, ['email' => $email,  'password' => $password ]);
			if ($result !== false && $result->num_rows == 1) {
				$row = $result->fetch_assoc();
				//Debug::log("logged: " . $_SESSION['user']);
				$this->updateUserSession(array(
					'id' => $row['id'],
					'email' => $email,
					'username' => $row['username']
				));
                //Debug::log("logged: " . $_SESSION['user']);
				DB::update(TABLE_USERS, ['lastlogin' => 'NOW()'], ['id' => $row['id']]);
				$this->redirect('index');
			} else {
				throw new Exception('Wrong password or email!');
			}
		} else {
			throw new Exception('Unable to connect to the database!');
		}
	}

    function createUserStatistics() {
	    return DB::insert(TABLE_USERS_STATISTICS, [
            'userId' => app()->getCurrentUserId()
        ]);
    }

	function updateUserSession($data) {
		$_SESSION['user'] = array_merge(isset($_SESSION['user']) ? $_SESSION['user'] : array(), $data);
	}

	function getCurrentUserId() {
		return $_SESSION['user']['id'];
	}

	function getPageTitle() {
		echo(cfg('pageTitle') . ' | ' . ucfirst($this->getPageName()));
	}

	function setPageName($pageName) {
		$this->pageName = $pageName;
	}

	function getPageName() {
		return $this->pageName;
	}

	function addCssFile($path, $vendor = false) {
		$this->cssFiles[] = array(
			'path' => $path,
			'vendor' => $vendor
		);
	}

	function getCssFiles() {
		return $this->cssFiles;
	}

	function addJsFile($path, $vendor = false) {
		$this->jsFiles[] = array(
			'path' => $path,
			'vendor' => $vendor
		);
	}

	function getJsFiles() {
		return $this->jsFiles;
	}

	function require_file($file, $once = false) {
        if (is_file($file)) {
            if ($once) {
                include_once $file;
            } else {
                include $file;
            }
        } else {
            echo('File "' . $file . '" not found!');
        }
	}

	public function setLayout($layout) {
		cfg('layout', $layout);
	}

	public function getLayout() {
		return cfg('layout');
	}

    public function getZero() {
        return 0;
    }

	public function getProps($props, $data) {
	    $result = array();
        foreach ($props as $prop) {
            if (isset($data[$prop])) {
               $result[$prop] = $data[$prop];
            }
	    }
	    return $result;
    }

}

function app() {
	static $app;
	if (!isset($app)) {
		$app = new Application();
	}
	return $app;
}

function controller() {
    return app()->getController();
}