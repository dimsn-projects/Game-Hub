<?php

class CFG {
	private static $variables = array();

	public static function & Variable($name, $value = null) {
		if ($name !== null && is_string($name)) {
			$name = strtolower($name);
		}
		if (is_string($name) && $name !== null && $value !== null) self::$variables[$name] = $value;
		return self::$variables[$name];
	}

	public static function & getVar($name) {
		return self::Variable($name, null);
	}

	public static function & setVar($name, $value) {
		return self::Variable($name, $value);
	}

	public static function isSetVar($name) {
		return isset(self::$variables[$name]);
	}
}

// config
function cfg($var, $value = null) {
	return CFG::Variable($var, $value);
}
