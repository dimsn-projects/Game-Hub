<?php

/**
 * autour: Ivan Andonov
 * @email: ivan[dot]andonov[at]gmail[dot]com
 * 
 * @require:
 * @use:
 **/

class Debug {
	
	// write message
	public static function log($str=null, $clear=false, $props=null, $file=null) {
		if (Debug::logging()) {
			$maxFileSize = Debug::maxFileSize();
			if (!$file) $file = Debug::filename() . '.' . Debug::extension();
			if ($clear || (is_file($file) && filesize($file) > $maxFileSize)) Debug::clear($file);
			if ($str != null) {
				if (!is_file($file) || $clear){
					$flag = "w+";
				} else {
					$flag = 'a+'; //$append ? 'a+' : 'r+';
				}
				$handle = fopen($file, $flag);
				$str = Debug::calcTime() . ": $str\n";
				if (is_array($props)) {
					foreach ($props as $key=>$val) {
						$str .= "$key=$val\n";
					}
				}
				if (Debug::isFirst()) {
					$str =	"==============================\n" . 
							"time: " . date('l, d F Y, G:i:s O') . "\n" .
							(isset($_SERVER['REMOTE_ADDR'])  ? ("IP: " . $_SERVER['REMOTE_ADDR'] . "\n") : "") .
							"------------------------------\n" .
							$str;
				}
				return fwrite($handle, $str);
			}
		}
	}
	// delete log file
	public static function clear($file=null) {
		if (!$file) $file = Debug::filename() . '.' . Debug::extension();
		if (is_file($file)) {
			return unlink($file);
		}
		return true;
	}
	
	// SETTERS/GETTERS
	// PUBLIC 
	public static function logging($value = null) {
		static $_logging;
		if (isset($value)) $_logging = $value;
		if ($_logging === null) $_logging = true;
		return $_logging;
	}
	public static function maxFileSize($value = null) {
		static $_size;
		if (isset($value)) $_size = $value * 1024 * 1024;
		if ($_size === null) $_size = 1024 * 1024;
		return $_size;
	}
	// get log file name
	public static function filename($value = null) {
		static $_filename;
		if (isset($value)) $_filename = $value;
		if ($_filename === null) {
			$_filename = '';
			if (isset($_SERVER["SCRIPT_FILENAME"])) {
				$filename = $_SERVER["SCRIPT_FILENAME"];
			} else if (count($_SERVER["argv"])) { // cron
				$filename = $_SERVER["argv"][0];
			}
			if (!strlen($filename)) {
				$filename = __FILE__;
			}
			$arr = explode('.', basename($filename));
			$_filename = array_shift($arr);
		}
		return $_filename;
	}
	public static function extension($ext = null) {
		static $_ext;
		if (isset($ext)) $_ext = $ext;
		if ($_ext === null) $_ext = 'log';
		return $_ext;
	}
	public static function showHours($bool = null) {
		static $_hours;
		if (isset($bool)) $_hours = $bool;
		if ($_hours === null) $_hours = true;
		return $_hours;
	}
	public static function showMinutes($bool = null) {
		static $_minutes;
		if (isset($bool)) $_minutes = $bool;
		if ($_minutes === null) $_minutes = true;
		return $_minutes;
	}
	// PRIVATE 
	public static function initTime($value = null) {
		static $_initTime;
		if (isset($value)) $_initTime = $value;
		return $_initTime;
	}
	public static function isFirst() {
		static $_initTime;
		$bool = $_initTime == null;
		$_initTime = true;
		return $bool;
	}
	
	// UTILS
	// get microtime
	public static function mtime() {
		return array_sum(explode(' ', microtime()));
	}
	public static function appTime() {
		return Debug::mtime() - Debug::initTime();
	}
	
	// calculate and format two microtimes
	public static function calcTime($checkTIme = null, $startTime = null) {
		return Debug::formatTime((isset($checkTIme) ? $checkTIme : Debug::mtime()) - (isset($startTime) ? $startTime : Debug::initTime()));
	}
	// format micro time to miliseconds
	public static function formatTime($time) {
		$time = number_format($time, 3, '.', '');
		if (Debug::showMinutes()) {
			$time = explode('.', number_format($time, 3, '.', ''));
			$secs = $time[0]%60;
			$mins = floor(($time[0] - $secs) / 60);
			if (Debug::showHours()) {
				$mins %= 60;
				$hours = floor(($time[0]-$mins*60-$secs)/(60*60))%24;
				return sprintf("%0" . max(strlen($hours), 2) . "s:%02s:%02s:%03s", $hours, $mins, $secs, $time[1]);
			} else {
				return sprintf("%0" . max(strlen($mins), 2) . "s:%02s:%03s", $mins, $secs, $time[1]);
			}
		} else {
			return $time;
		}
	}
}

Debug::initTime(Debug::mtime());