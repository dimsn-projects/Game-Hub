<?php

function isInteger($value) {
	$value2 = intval($value);
	return "$value" == "$value2";
}

function moveUploadedFiles(&$files = []) {

    if (!file_exists(UPLOAD_DIR)) {
        mkdir(UPLOAD_DIR, 0644, true);
    }
    foreach ($_FILES as $name => $file) {
        if (is_array($file['name'])) {
            for ($i = 0; $i < count($file['name']); $i++) {
                $tmpPath = $file['tmp_name'][$i];
                if (is_file($tmpPath)) {
                    if ($path = tempnam(UPLOAD_DIR, '')) {
                        move_uploaded_file($tmpPath, $path);
                        $files[] = [
                            'name' => $file['name'][$i],
                            'path' => $path
                        ];
                    }
                }
            }
        } else {
            $tmpPath = $file['tmp_name'];
            if (is_file($tmpPath)) {
                if ($path = tempnam(UPLOAD_DIR, '')) {
                    move_uploaded_file($tmpPath, $path);
                    $files[] = [
                        'name' => $file['name'],
                        'path' => $path
                    ];
                }
            }
        }
    }
}
function getDefaultArgumentValue($reflection, $argumentName) {
    foreach ($reflection->getParameters() as $param) {
        if ($param->name === $argumentName) {
            return $param->getDefaultValue();
        }
    }
    return null;
}